import {send} from 'micro';
import {router, post, get} from 'microrouter';
import pMap from 'p-map';
import database from './config/database';

import crawling from './controllers/crawler';
import getLocation from './lib/locations';

import queue from './lib/queue';
export default router(
  post('/', crawling),
  get('/:status/:id_marketplace/:keyword_id', async (req, res) => {
    const keyword = await database.getKeywordById(req.params.keyword_id);
    if (keyword) {
      if (req.params.status === 'active') {
        const marketplace = await database.getMarketplaceById(
          String(keyword.idMarketplace)
        );
        if (marketplace) {
          const queueInstance = queue.create(keyword.keyword, marketplace);

          if (queueInstance.isPaused()) {
            const previousJob = await queueInstance.getJob(
              `${marketplace}__${keyword.keyword}`
            );
            if (!previousJob) {
              const locations = keyword.location.split(',');
              await pMap(locations, async location => {
                const pLocation = getLocation(location, marketplace as any);
                if (pLocation) {
                  await queueInstance.add(
                    `${marketplace}:${pLocation.id}:${keyword.keyword}`,
                    {
                      location: pLocation,
                      page: 0,
                      id_marketplace: keyword.idMarketplace,
                      id_keyword: keyword.idKeyword,
                      id_commodity: keyword.idCommodity,
                      firstPage: true,
                      created_by: String(keyword.createdBy),
                      query: keyword.keyword,
                    }
                  );
                  await queue.createWorker(keyword.keyword, marketplace);
                  await queueInstance.resume();
                  console.log('QUEUE HAS BEEN STARTED');
                }
              });
            } else {
              await queueInstance.resume();
              await queue.createWorker(keyword.keyword, marketplace);
            }
          }
        }
      } else {
        console.log('QUEUE HAS BEEN PAUSED');

        const marketplace = await database.getMarketplaceById(
          String(keyword.idMarketplace)
        );
        if (marketplace) {
          const isAvailable = queue.getQueue(keyword.keyword, marketplace);
          if (isAvailable) {
            await isAvailable.pause();
          }

          const worker = queue.getWorker(keyword.keyword, marketplace);
          if (worker) {
            await worker.queueEvents.close();
            await worker.queueEvents.disconnect();
            await worker.worker.disconnect();
            await worker.worker.close();
            await worker.queueScheduler.disconnect();
            await worker.queueScheduler.close();
          }
        }
      }
    }

    return send(res, 200, {
      code: 200,
      success: true,
      message: 'Berhasil',
    });
  })
);
