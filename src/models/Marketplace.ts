import { Model } from 'objection';

class Marketplace extends Model {
  static tableName = 'marketplace';
  static idColumn = 'id_marketplace';

  public id_marketplace!: number;
  public name!: string;
  public description!: string;

  static get jsonSchema() {
    return {
      type: 'object',
      required: ['name'],

      properties: {
        id_marketplace: { type: 'integer' },
        name: { type: 'string' },
        description: { type: 'string', minLength: 1 },
      },
    };
  }
}

export default Marketplace;
