import {
  DataProduct as ModelDataProduct,
  Product as ModelProduct,
  Store as ModelStore,
} from '../';
import type {PartialModelObject} from 'objection';

export interface SaveParams {
  store: Omit<
    Omit<
      PartialModelObject<ModelStore>,
      'id_store' | 'created_at' | 'updated_at'
    >,
    'QueryBuilderType'
  >;
  product: Omit<
    Omit<
      PartialModelObject<ModelProduct>,
      'id_product' | 'id_store' | 'created_at' | 'updated_at'
    >,
    'QueryBuilderType'
  >;
  data_product: Omit<
    Omit<
      PartialModelObject<ModelDataProduct>,
      'id_data_product' | 'id_product' | 'created_at' | 'updated_at'
    >,
    'QueryBuilderType'
  >;
  url: string;
  created_by?: number;
}
