import { Model } from 'objection';
import Link from './Link';

class ReportedProduct extends Model {
  static tableName = 'reported_products';
  static idColumn = 'id_reported_product';

  public id_reported_product!: number;
  public id_user!: number;
  public id_link!: number;
  public id_status!: number;
  public initial!: string;
  public periode!: number;
  public filename!: string;
  public notes!: string;
  public link!: string;
  public id_product!: number;
  public updated_at!: string;

  static get jsonSchema() {
    return {
      type: 'object',

      properties: {
        id_reported_product: { type: 'integer' },
        id_link: { type: 'number' },
        id_status: { type: 'number' },
        filename: { type: 'string', minLength: 1 },
      },
    };
  }

  static get relationMappings() {
    return {
      links: {
        relation: Model.HasManyRelation,
        modelClass: Link,
        join: {
          from: 'reported_products.id_link',
          to: 'links.id',
        },
      },
    };
  }
}

export default ReportedProduct;
