import { Model } from 'objection';

class DataProduct extends Model {
  static tableName = 'data_product';
  static idColumn = 'id_data_product';

  public id_product!: number;
  public price!: string;
  public crawled!: string;
  public rating!: number;
  public discount!: number;
  public item_count!: number;
  public sold_count!: number;
  public view_count!: number;
  public favorited_count!: number;
  public processing_time!: string;
  public item_condition!: string;
  public keywords!: string;
  public manufacturer!: string;
  public created_at!: number;
  public updated_at!: number;
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['id_product'],

      properties: {
        id_data_product: { type: 'integer' },
        id_product: { type: 'number' },
      },
    };
  }
}

export default DataProduct;
