import { Model } from 'objection';

class Store extends Model {
  public id_store!: number;
  public created_at!: string;
  public updated_at!: string;
  public id_marketplace!: number;
  public url!: string;
  public name!: string;
  public location!: string;
  public description!: string;
  public joined_since!: string;
  public last_update!: string;
  public seller_rate!: string;
  public seller_type!: string;
  public brand!: string;
  public crawled_from!: string;
  public total_product!: number;
  public target!: number;
  public followers!: number;

  static tableName = 'stores';
  static idColumn = 'id_store';
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['id_marketplace', 'url'],

      properties: {
        id_store: { type: 'integer' },
        id_marketplace: { type: 'number' },
        url: { type: 'string', minLength: 1, maxLength: 255 },
      },
    };
  }
}

export default Store;
