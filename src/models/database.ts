import {Model} from 'objection';
import Knex from 'knex';
import type {Knex as KnexConfig} from 'knex';
import {
  DataProduct,
  Link,
  Product,
  Store,
  Marketplace,
  Keyword,
  ReportedProduct,
} from '../models';

import dayjs from 'dayjs';
import isToday from 'dayjs/plugin/isToday';
import {SaveParams} from './types/save';

dayjs.extend(isToday);

type GetKeyword = {
  keyword: string;
  id_marketplace: number;
  id_commodity: number;
  product_origin?: string;
  max_product?: string;
  note?: string;
  status?: string;
  therapy_class?: string;
  group?: string;
  product_total?: number;
  created_by?: number;
  locations?: string;
};

/**
 * Class untuk database
 */
class Database {
  private _knex: KnexConfig;
  constructor(config: KnexConfig.Config['connection']) {
    this._knex = Knex({
      client: 'mysql2',
      connection: config,
      pool: {
        min: 2,
        max: 15,
      },
    });

    Model.knex(this._knex);
  }

  public async save(data: SaveParams) {
    try {
      const insert = await Store.transaction(async trx => {
        const {store: s, product: p, data_product: dp} = data;

        const store = await Store.query(trx)
          .insert(s)
          .onConflict('url')
          .merge();

        if (store.id_store === 0) {
          const {id_store} = await Store.query(trx)
            .select('id_store')
            .orderBy('created_at', 'desc')
            .where({
              url: s.url,
            })
            .first();
          store.id_store = id_store;
        }

        const product = await Product.query(trx)
          .insert({
            id_store: store.id_store,
            ...p,
          })
          .onConflict('url')
          .merge();
        if (product.id_product === 0) {
          const {id_product} = await Product.query(trx)
            .select('id_product')
            .orderBy('created_at', 'desc')
            .where({
              url: p.url,
            })
            .first();
          product.id_product = id_product;
        }

        const prevDataProduct = await DataProduct.query(trx)
          .select('created_at')
          .where({
            id_product: product.id_product,
          })
          .andWhere('created_at', 'like', `%${dp.crawled.split(' ')[0]}%`)
          .orderBy('created_at', 'desc')
          .first();

        if (prevDataProduct && dayjs(prevDataProduct.created_at).isToday()) {
          await DataProduct.query(trx)
            .where({id_product: product.id_product})
            .update({
              id_product: product.id_product,
              ...dp,
            });
        } else {
          await DataProduct.query(trx).insert({
            id_product: product.id_product,
            ...dp,
          });
        }

        await Link.query(trx)
          .insert({
            id_product: product.id_product,
            id_status: 1,
            id_user: data.created_by,
            link: data.url,
          })
          .onConflict('url')
          .merge();

        return product.id_product;
      });

      console.log(`Berhasil menyimpan ke database: ${data.url}`);

      return insert;
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public async getMarketplaceId(marketplaceName: string) {
    try {
      const {id_marketplace} = await Marketplace.query()
        .select('id_marketplace')
        .where({
          name: marketplaceName,
        })
        .first();

      return id_marketplace;
    } catch (error) {
      console.log(error);
      throw new Error(error.message);
    }
  }
  public async getMarketplaceById(marketplaceId: string) {
    try {
      const {name} = await Marketplace.query()
        .select('name')
        .where({
          id_marketplace: marketplaceId,
        })
        .first();

      return name;
    } catch (error) {
      console.log(error);
      throw new Error(error.message);
    }
  }

  public async getKeyword(params: GetKeyword) {
    try {
      const isExists = await Keyword.query()
        .select('id_keywords')
        .where({
          keywords: params.keyword,
          id_marketplace: params.id_marketplace,
          id_commodity: params.id_commodity,
          product_origin: params.product_origin ?? null,
          id_user: params.created_by || null,
          status: '1',
        })
        .first();

      if (!isExists) {
        const insert = await Keyword.query().insert({
          id_marketplace: params.id_marketplace,
          id_commodity: params.id_commodity,
          keywords: params.keyword,
          status: '1',
          max_product: params.max_product || '-1',
          note: params.note,
          therapy_class: params.therapy_class,
          group: params.group,
          product_origin: params.product_origin,
          product_total: params.product_total || 0,
          id_user: params.created_by,
          locations: params.locations,
        });

        return insert.id_keywords;
      }
      return isExists.id_keywords;
    } catch (error) {
      console.log(error);
      throw new Error(error.message);
    }
  }

  public async getKeywordExists(params: GetKeyword) {
    try {
      const isExists = await Keyword.query()
        .select('id_keywords')
        .where({
          keywords: params.keyword,
          id_marketplace: params.id_marketplace,
          id_commodity: params.id_commodity,
          product_origin: params.product_origin ?? null,
          id_user: params.created_by || null,
          status: '1',
        })
        .first();

      if (!isExists) {
        return false;
      }
      return true;
    } catch (error) {
      throw new Error(error);
    }
  }

  public async getKeywords() {
    try {
      const keyword = await Keyword.query().select().where({
        status: 1,
      });

      return keyword.map(k => ({
        id_user: k.id_user,
        id_keyword: k.id_keywords,
        id_commodity: k.id_commodity,
        id_marketplace: k.id_marketplace,
        keyword: k.keywords,
        product_origin: k.product_origin,
        max_product: k.max_product,
        created_at: k.created_at,
        locations: k.locations,
      }));
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public async getKeywordByStatus(status: string | number) {
    try {
      const keywords = await Keyword.query().select('id_keywords').where({
        status,
      });

      return keywords.map(keyword => ({
        id_keyword: keyword.id_keywords,
      }));
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public async getKeywordById(keywordId: string | number) {
    try {
      const keywords = await Keyword.query()
        .select(
          'id_keywords',
          'id_commodity',
          'keywords',
          'id_user',
          'id_marketplace',
          'locations'
        )
        .where({
          id_keywords: keywordId,
        })
        .first();

      return {
        keyword: keywords.keywords,
        idMarketplace: keywords.id_marketplace,
        idKeyword: keywords.id_keywords,
        idCommodity: keywords.id_commodity,
        createdBy: keywords.id_user,
        location: keywords.locations,
      };
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public async updateProductTotal(idKeyword: number, total: number) {
    try {
      await Keyword.query()
        .where({
          id_keywords: idKeyword,
        })
        .update({
          product_total: total,
        })
        .first();
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public async updateKeywordStatus(
    idKeyword: string | number,
    status: string | number
  ) {
    try {
      await Keyword.query()
        .where({
          id_keywords: idKeyword,
        })
        .update({
          status,
        });
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public async getReportedProduct(
    name: string
  ): Promise<
    {link: string; id_reported_product: number; id_product: number}[]
  > {
    try {
      const reportedProducts = await ReportedProduct.query()
        .select(
          'links.link',
          'reported_products.id_reported_product',
          'links.id_product'
        )
        .joinRelated('links')
        .where('reported_products.id_status', 1)
        .whereRaw(`LOCATE("${name}", link) > 0`);

      return reportedProducts.map(r => ({
        id_reported_product: r.id_reported_product,
        id_product: r.id_product,
        link: r.link,
      }));
    } catch (error) {
      throw new Error(error.message);
    }
  }

  public async toggleStatusReportedProduct(
    reportedProductId: string | number,
    statusId?: 1 | 5
  ) {
    try {
      await ReportedProduct.query()
        .update({
          id_status: statusId || 1,
          updated_at: dayjs().format('YYYY-MM-DD HH:mm:ss'),
        })
        .where({
          id_reported_product: reportedProductId,
        });

      const reported = await ReportedProduct.query()
        .select('id_link')
        .where({
          id_reported_product: reportedProductId,
        })
        .first();

      if (reported) {
        const idLink = reported.id_link;
        await Link.query()
          .update({
            id_status: statusId || 1,
            updated_at: dayjs().format('YYYY-MM-DD HH:mm:ss'),
          })
          .where({
            id: idLink,
          });
      }
    } catch (error) {
      throw new Error(error.message);
    }
  }
  public async close() {
    this._knex.destroy();
  }
}

export default Database;
