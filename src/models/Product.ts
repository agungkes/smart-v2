import { Model } from 'objection';

class Product extends Model {
  public id_product!: number;
  public id_store!: number;
  public id_keywords!: number;
  public id_commodity!: number;
  public name!: string;
  public description!: string;
  public category!: string;
  public category_url!: string;
  public picture!: string;
  public screenshot!: string;
  public note!: string;
  public created_at!: number;
  public updated_at!: number;
  public url!: string;
  static tableName = 'products';
  static idColumn = 'id_product';
  static get jsonSchema() {
    return {
      type: 'object',
      required: ['id_store', 'id_keywords', 'id_commodity', 'name'],

      properties: {
        id_product: { type: 'integer' },
        id_store: { type: 'integer' },
        id_keywords: { type: 'integer' },
        id_commodity: { type: 'integer' },
        name: { type: 'string', minLength: 1, maxLength: 255 },
      },
    };
  }
}

export default Product;
