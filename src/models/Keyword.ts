import { Model } from 'objection';

class Keyword extends Model {
  static tableName = 'keywords';
  static idColumn = 'id_keywords';

  public id_keywords!: number;
  public id_marketplace!: number;
  public id_commodity!: number;
  public id_user!: number;
  public keywords!: string;
  public status!: number;
  public locations!: string;
  public max_product!: string;
  public note!: string;
  public content!: string;
  public therapy_class!: string;
  public group!: string;
  public product_origin!: string;
  public product_total!: number;
  public created_at!: string;

  static get jsonSchema() {
    return {
      type: 'object',

      properties: {
        id_keywords: { type: 'integer' },
        id_marketplace: { type: 'integer' },
        keywords: { type: 'string' },
        status: { type: 'string' },
        max_product: { type: 'string' },
        note: { type: 'string' },
        content: { type: 'string' },
        therapy_class: { type: 'string' },
        group: { type: 'string' },
        product_origin: { type: 'string' },
        product_total: { type: 'integer' },
      },
    };
  }
}

export default Keyword;
