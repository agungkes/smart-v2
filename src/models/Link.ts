import { Model } from 'objection';

class Link extends Model {
  static tableName = 'links';
  static idColumn = 'id';

  public id!: number;
  public id_product!: number;
  public id_status!: number;
  public id_offense!: number;
  public id_user!: number;
  public link!: string;
  public note!: string;
  public inserted_by!: string;
  public updated_at!: string;

  static get jsonSchema() {
    return {
      type: 'object',

      properties: {
        id: { type: 'integer' },
        id_product: { type: 'number' },
        id_status: { type: 'number' },
        link: { type: 'string', minLength: 1 },
      },
    };
  }
}

export default Link;
