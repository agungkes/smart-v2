import {config as dotenvConfig} from 'dotenv';
dotenvConfig();

import gracefulShutdown from 'http-graceful-shutdown';
import micro from 'micro';
import pMap from 'p-map';
import database from './config/database';
import redis from './config/redis';
import getLocation from './lib/locations';
import {releasePuppeteerPool} from './lib/puppeteerPool';
import queue from './lib/queue';
import routes from './routes';

process.setMaxListeners(30);

const APP_PORT = process.env.APP_PORT || 3000;
const server = micro(routes);

server.listen(APP_PORT, () => {
  console.log(`Service listen on 127.0.0.1:${APP_PORT}`);
  // initial();
  if (process.send) {
    process.send('ready');
  }
});

const initial = async () => {
  const keywords = await database.getKeywords();
  if (keywords) {
    console.log('GENERATE INITIAL JOB AFTER RESTART!');

    await pMap(keywords, async keyword => {
      const marketplace = await database.getMarketplaceById(
        String(keyword.id_marketplace)
      );
      if (marketplace) {
        const locations = keyword.locations.split(',');
        await pMap(locations, async location => {
          const pLocation = getLocation(location, marketplace as any);
          if (pLocation) {
            const queueInstance = queue.create(keyword.keyword, marketplace);
            await queueInstance.add(
              `${marketplace}:${pLocation.id}:${keyword.keyword}`,
              {
                location: pLocation,
                page: 0,
                id_marketplace: keyword.id_marketplace,
                id_keyword: keyword.id_keyword,
                id_commodity: keyword.id_commodity,
                firstPage: true,
                created_by: keyword.id_user,
                query: keyword.keyword,
              }
            );

            await queue.createWorker(keyword.keyword, marketplace);

            const timestamp = new Date();
            console.log(
              `[${timestamp.getTime()}] Melakukan crawler dengan kata kunci ${
                keyword.keyword
              } pada marketplace ${keyword.id_marketplace} di lokasi ${
                keyword.locations
              }`
            );
          } else {
            const timestamp = new Date();
            console.log(
              `[${timestamp.getTime()}] Lokasi tidak ditemukan di marketplace ${marketplace} - ${location}`
            );
          }
        });
      }
    });
  }
};

const preShutdown = async (signal?: string) => {
  // Pause all queue
  const listOfQueue = queue.getAllQueues();
  Object.keys(listOfQueue).forEach(async key => {
    await listOfQueue[key].pause();
  });

  // Stop worker
  const listOfWorkers = queue.getAllWorker();
  Object.keys(listOfWorkers).forEach(async key => {
    await listOfWorkers[key].worker.close();
    await listOfWorkers[key].queueEvents.close();
    await listOfWorkers[key].queueScheduler.close();
  });

  const keywords = await database.getKeywords();
  if (keywords) {
    await pMap(keywords, async keyword => {
      await database.updateKeywordStatus(String(keyword.id_keyword), '0');
    });
  }

  redis.disconnect();
  await releasePuppeteerPool();
  await database.close();
};

gracefulShutdown(server, {
  signals: 'SIGINT SIGTERM SIGTERM2',
  timeout: 10000, // timeout: 10 secs
  development: true, // not in dev mode
  forceExit: true, // triggers process.exit() at the end of shutdown process
  preShutdown: preShutdown, // needed operation before httpConnections are shutted down
});
