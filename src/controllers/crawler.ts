import type {IncomingMessage} from 'http';
import {buffer, send} from 'micro';
import type {ServerResponse} from 'microrouter';
import pMap from 'p-map';
import {parse} from 'querystring';
import database from '../config/database';
import getLocation from '../lib/locations';
import queue from '../lib/queue';

const crawling = async (req: IncomingMessage, res: ServerResponse) => {
  try {
    const bufferData = await buffer(req);
    const parsedData = parse(bufferData.toString()) as unknown as RequestParams;

    const idKeywords = parsedData.id_keywords.split(',');
    await pMap(idKeywords, async idKeyword => {
      const dbKeyword = await database.getKeywordById(idKeyword);
      if (dbKeyword) {
        const marketplace = await database.getMarketplaceById(
          String(dbKeyword.idMarketplace)
        );
        if (marketplace) {
          const locations = dbKeyword.location.split(',');
          await pMap(locations, async location => {
            const pLocation = getLocation(location, marketplace as any);

            // Jika lokasi tidak ditemukan maka tidak perlu melakukan pencarian
            if (pLocation) {
              const queueInstance = queue.create(
                dbKeyword.keyword,
                marketplace
              );

              await queueInstance.add(
                `${marketplace}:${pLocation.id}:${dbKeyword.keyword}`,
                {
                  ...parsedData,
                  location: pLocation,
                  page: 0,
                  id_marketplace: dbKeyword.idMarketplace,
                  id_keyword: idKeyword,
                  id_commodity: dbKeyword.idCommodity,
                  firstPage: true,
                }
              );

              await queue.createWorker(dbKeyword.keyword, marketplace);

              const timestamp = new Date();
              console.log(
                `[${timestamp.getTime()}] Melakukan crawler dengan kata kunci ${
                  dbKeyword.keyword
                } pada marketplace ${dbKeyword.idMarketplace} di lokasi ${
                  dbKeyword.location
                }`
              );
            } else {
              const timestamp = new Date();
              console.log(
                `[${timestamp.getTime()}] Lokasi tidak ditemukan di marketplace ${marketplace} - ${location}`
              );
            }
          });
        }
      }
    });

    return send(res, 200, {
      code: 200,
      success: true,
      message: 'Berhasil menjalankan crawler',
    });
  } catch (error) {
    return send(res, 403, {
      code: 403,
      success: false,
      message: error.message,
    });
  }
};

export default crawling;
