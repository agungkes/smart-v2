import getRandom from './random';

export const PROXY = (String(process.env.PROXY) || '').split(',');

const getProxy = () => {
  return PROXY.length > 0
    ? `${getRandom(PROXY)}:${process.env.PROXY_PORT}`
    : undefined;
};

export default getProxy;
