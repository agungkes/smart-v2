const random = (min: number, max: number) =>
  Math.floor(Math.random() * max) + min;

const getRandom = (str: string[] = []) => {
  return str.length > 0 ? str[random(0, str.length - 1)] : '';
};

export default getRandom;
