import fs from 'fs';
import path from 'path';
import crypto from 'crypto';
import {Page} from 'puppeteer';
import exists from './exists';

const screenshot = async (page: Page, url: string, fullPage = false) => {
  await page.evaluate(text => {
    const bodyElement = document.querySelector('body');
    const captionElement = document.createElement('H2');
    captionElement.setAttribute(
      'style',
      'padding: 5px 30px; position: fixed; top: 0; left: 0; right: 0; background-color: white; z-index: 9999; font-size: 18px; font-weight: bold'
    );

    const t = document.createTextNode(text);
    captionElement.appendChild(t);

    if (bodyElement) {
      bodyElement.insertBefore(captionElement, bodyElement.children[1]);
      window.scrollTo(0, 0);
    }
  }, url);

  const screenshotDir = path.join('screenshots');
  const isScreenshotDirExists = await exists(screenshotDir);
  if (!isScreenshotDirExists) {
    fs.promises.mkdir(screenshotDir);
  }
  const filename = crypto.createHash('md5').update(url).digest('hex');
  const file = `${screenshotDir}/${filename}.jpg`;
  await page.screenshot({
    type: 'png',
    path: file,
    fullPage: fullPage,
  });

  return file;
};

export default screenshot;
