import {promises, constants} from 'fs';

async function exists(file: string) {
  return promises
    .access(file, constants.F_OK)
    .then(() => true)
    .catch(() => false);
}

export default exists;
