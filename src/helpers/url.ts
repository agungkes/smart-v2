const url = (text: string) => text.toLowerCase().replace(/\?[^\\]+/g, '');

export default url;
