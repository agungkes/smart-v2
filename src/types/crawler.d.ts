interface IRequestParams {
  marketplaces: string;
  commodity: string | number;
  product_origin: string;
  query: string;
  max?: string | number;
  crawl_by: string;
  location: string;
  created_by: string | number;
  id_keywords: string | number;
}

interface RequestParams {
  marketplaces: string;
  commodity: string | number;
  note: string;
  product_origin: string;
  query: string;
  max: string | number;
  crawl_by: string;
  location: string;
  created_by: string;
  id_keywords: string;
}
interface JobData {
  created_by: string | number;
  id_marketplace: string | number;
  query: string;
  location: {
    id: string;
    name: string;
    type: string;
  };
  created_by: string;
  id_keyword: string | number;
  id_commodity: string | number;
  page: string | number;
  firstPage?: boolean;
}
