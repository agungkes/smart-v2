export interface ShopeeShop {
  data: Data;
  error: null | string;
  error_msg: null | string;
}
interface Data {
  place: null;
  ctime: number;
  mtime: number;
  country: string;
  last_active_time: number;
  is_shopee_verified: boolean;
  is_official_shop: boolean;
  chat_disabled: boolean;
  disable_make_offer: number;
  enable_display_unitno: null;
  cover: string;
  rating_normal: number;
  rating_bad: number;
  rating_good: number;
  shop_covers: ShopCover[];
  description: string;
  is_semi_inactive: boolean;
  is_blocking_owner: null;
  preparation_time: number;
  cancellation_rate: number;
  followed: boolean;
  buyer_rating: BuyerRating;
  vacation: boolean;
  show_low_fulfillment_warning: boolean;
  show_official_shop_label: boolean;
  show_official_shop_label_in_title: boolean;
  show_shopee_verified_label: boolean;
  show_official_shop_label_in_normal_position: null;
  real_url_if_matching_custom_url: null;
  status: number;
  cb_option: number;
  campaign_id: null;
  has_decoration: boolean;
  shop_location: string;
  rating_star: number;
  is_ab_test: null;
  show_live_tab: boolean;
  has_flash_sale: boolean;
  userid: number;
  shopid: number;
  name: string;
  item_count: number;
  follower_count: number;
  response_rate: number;
  response_time: number;
  account: Account;
  campaign_config: null;
  has_shopee_flash_sale: boolean;
  has_in_shop_flash_sale: boolean;
  has_brand_sale: boolean;
  is_preferred_plus_seller: boolean;
  show_new_arrival_items: boolean;
  new_arrival_items_start_ts: number;
  visible_regions: null;
  block_cb_user: null;
  address: Address;
  priority_flash_sale_group: null;
}

interface Account {
  username: string;
  following_count: number;
  portrait: string;
  is_seller: boolean;
  phone_verified: boolean;
  email_verified: boolean;
  fbid: string;
  total_avg_star: number;
  hide_likes: number;
  feed_account_info: FeedAccountInfo;
  status: number;
}

interface FeedAccountInfo {
  is_kol: boolean;
  can_post_feed: boolean;
}

interface Address {
  id: number;
  userid: number;
  name: string;
  phone: string;
  region: string;
  state: string;
  city: string;
  address: string;
  status: number;
  ctime: number;
  mtime: number;
  zipcode: string;
  deftime: number;
  full_address: string;
  district: string;
  town: string;
  logistics_status: number;
  icno: string;
}

interface BuyerRating {
  rating_count: null;
  rating_star: null;
}

interface ShopCover {
  image_url: string;
  type: number;
  video_url: string;
  redirect_url: null;
  redirect_url_type: null;
}
