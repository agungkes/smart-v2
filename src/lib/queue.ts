import {JobsOptions, Queue, QueueEvents, QueueScheduler, Worker} from 'bullmq';
// import cronTime from 'cron-time-generator';
import redis from '../config/redis';

const currentDate = new Date();
export const defaultJobOptions: JobsOptions = {
  attempts: 3,
  backoff: {
    type: 'exponential',
    delay: 60000, // 1 minute
  },
  repeat: {
    cron: `${currentDate.getHours()} ${currentDate.getMinutes()} * * *`,
  },
  removeOnComplete: true,
};

const listOfQueue: {[key: string]: Queue<JobData>} = {};
const listOfWorkers: {
  [key: string]: {
    worker: Worker;
    queueEvents: QueueEvents;
    queueScheduler: QueueScheduler;
  };
} = {};

export default {
  getAllQueues: () => listOfQueue,
  getQueue: (name: string, marketplace: string) => {
    return listOfQueue[`${marketplace}`];
  },
  create: (name: string, marketplace: string) => {
    const queueName = `${marketplace}`;

    // Tidak usah buat baru jika sudah ada dengan nama yang sama
    if (listOfQueue[queueName]) {
      return listOfQueue[queueName];
    }

    const queueInstance = new Queue<JobData>(queueName, {
      connection: redis,
      defaultJobOptions: {
        ...defaultJobOptions,
        jobId: marketplace + '__' + name,
      },
    });

    listOfQueue[queueName] = queueInstance;
    return queueInstance;
  },
  pause: (name: string, marketplace: string) => {
    return listOfQueue[`${marketplace}:${name}`].pause();
  },
  resume: (name: string, marketplace: string) => {
    return listOfQueue[`${marketplace}:${name}`].resume();
  },
  createWorker: async (name: string, marketplace: string) => {
    const workerName = `${marketplace}`;
    if (!listOfWorkers[workerName]) {
      const processor = await (
        await import(`../services/${marketplace}/getFromPage`)
      ).default;

      const worker = new Worker(workerName, processor, {concurrency: 1});
      const queueScheduler = new QueueScheduler(workerName);
      const queueEvents = new QueueEvents(workerName);

      listOfWorkers[workerName] = {
        worker,
        queueScheduler,
        queueEvents,
      };

      worker.on('completed', job => {
        console.log(`[${marketplace}] ${job.id} has completed!`);
      });

      worker.on('failed', (job, err) => {
        console.log(
          `[${marketplace}] ${job.id} has failed with ${err.message}`
        );
      });

      queueEvents.on('waiting', ({jobId}) => {
        console.log(`[${marketplace}] A job with ID ${jobId} is waiting`);
      });

      queueEvents.on('active', ({jobId, prev}) => {
        console.log(
          `[${marketplace}] Job ${jobId} is now active; previous status was ${prev}`
        );
      });

      queueEvents.on('completed', ({jobId, returnvalue}) => {
        console.log(
          `[${marketplace}] ${jobId} has completed and returned ${returnvalue}`
        );
      });

      queueEvents.on('failed', ({jobId, failedReason}) => {
        console.log(
          `[${marketplace}] ${jobId} has failed with reason ${failedReason}`
        );
      });

      queueEvents.on('progress', ({jobId, data}, timestamp) => {
        console.log(
          `[${marketplace}] ${jobId} reported progress ${data} at ${timestamp}`
        );
      });
    }
  },
  getAllWorker: () => {
    return listOfWorkers;
  },
  getWorker: (name: string, marketplace: string) => {
    const workerName = `${marketplace}`;
    return listOfWorkers[workerName];
  },
};
