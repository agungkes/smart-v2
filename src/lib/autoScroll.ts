import {Page} from 'puppeteer';

const autoScroll = async (page: Page) => {
  const runAutoScroll = (distance: number, interval: number) =>
    new Promise<void>(resolve => {
      let totalHeight = 0;
      const timer = setInterval(() => {
        const scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        totalHeight += distance;

        if (totalHeight >= scrollHeight) {
          clearInterval(timer);
          resolve();
        }
      }, interval);
    });

  await page.evaluate(runAutoScroll, 300, 300);
};

export default autoScroll;
