import MiniSearch from 'minisearch';
import Blibli from './blibli.json';
import Bukalapak from './bukalapak.json';
import Elevenia from './elevenia.json';
import Lazada from './lazada.json';
import Olx from './olx.json';
import Shopee from './shopee.json';
import Tokopedia from './tokopedia.json';
import Jdid from './jdid.json';

const locationIndex = new MiniSearch({
  fields: ['id', 'name', 'category', 'type'],
  storeFields: ['id', 'name', 'category', 'type'],
  searchOptions: {
    prefix: true,
  },
});

type Data = {
  id: string | number;
  name: string;
};
type Marketplace =
  | 'blibli'
  | 'bukalapak'
  | 'elevenia'
  | 'lazada'
  | 'olx'
  | 'shopee'
  | 'tokopedia'
  | 'jdid';
const addCategory = (data: Data[], category: Marketplace) =>
  data.map(d => ({...d, category}));

locationIndex.addAll([
  ...addCategory(Blibli, 'blibli'),
  ...addCategory(Bukalapak, 'bukalapak'),
  ...addCategory(Elevenia, 'elevenia'),
  ...addCategory(Lazada, 'lazada'),
  ...addCategory(Olx, 'olx'),
  ...addCategory(Shopee, 'shopee'),
  ...addCategory(Tokopedia, 'tokopedia'),
  ...addCategory(Jdid, 'jdid'),
]);

const getLocation = (query: string, marketplace: Marketplace | string) => {
  const result = locationIndex.search(query, {
    filter: res => res.category === marketplace && res.score > 1,
  });

  return result.length > 0
    ? {id: result[0].id, name: result[0].name, type: result[0].type ?? ''}
    : undefined;
};

export default getLocation;
