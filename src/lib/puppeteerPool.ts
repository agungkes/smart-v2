// eslint-disable-next-line
// @ts-nocheck

import genericPool, {Factory, Options} from 'generic-pool';
import {Browser} from 'puppeteer';

import puppeteerExtra from 'puppeteer-extra';
import StealthPlugin from 'puppeteer-extra-plugin-stealth';
import UserDataDir from 'puppeteer-extra-plugin-user-data-dir';
import AnonymizeUa from 'puppeteer-extra-plugin-anonymize-ua';

puppeteerExtra.use(StealthPlugin());
puppeteerExtra.use(UserDataDir());
puppeteerExtra.use(AnonymizeUa());

const factory: Factory<Browser> = {
  create: async function () {
    try {
      const browser = await puppeteerExtra.launch({
        // headless: process.env.APP_ENV === 'production' || false,
        userDataDir: '/tmp/smart-crawler-engine/data',
        defaultViewport: {
          width: 1280,
          height: 882,
        },
        ignoreDefaultArgs: true,
        args: [
          '--headless',
          '--hide-scrollbars',
          '--mute-audio',
          '--disable-canvas-aa', // Disable antialiasing on 2d canvas
          '--disable-2d-canvas-clip-aa', // Disable antialiasing on 2d canvas clips
          '--disable-gl-drawing-for-tests', // BEST OPTION EVER! Disables GL drawing operations which produce pixel output. With this the GL output will not be correct but tests will run faster.
          '--disable-dev-shm-usage', // ???
          '--no-zygote', // wtf does that mean ?
          '--use-gl=desktop', // better cpu usage with --use-gl=desktop rather than --use-gl=swiftshader, still needs more testing.
          '--enable-webgl',
          // '--no-first-run',
          '--disable-infobars',
          '--disable-breakpad',
          '--ignore-gpu-blacklist',
          '--disable-gpu',
          '--disable-software-rasterizer',
          '--no-sandbox', // meh but better resource comsuption
          '--disable-setuid-sandbox', // same
        ],
      });
      return browser;
    } catch (error) {
      throw new Error(error.message);
    }
  },
  destroy: async function (client) {
    await client.close();
  },
};

const opts: Options = {
  max: 8, // maximum size of the pool
  min: 2, // minimum size of the pool
};

const puppeteerPool = genericPool.createPool(factory, opts);
export const releasePuppeteerPool = async () => {
  await puppeteerPool.drain();
  await puppeteerPool.clear();
};
export default puppeteerPool;
