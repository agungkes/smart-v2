import IORedis from 'ioredis';

const redis = new IORedis({
  host: process.env.REDIS_HOST || 'localhost',
  port: Number(process.env.REDIS_PORT) || 6379,
  enableReadyCheck: true,
  username: process.env.REDIS_USER || 'default',
  password: process.env.REDIS_PASS || '',
});

redis.on('ready', () => {
  console.log('Redis server is ready.');
});

redis.on('error', error => {
  console.log('Error in Redis server: ' + error);
});

export default redis;
