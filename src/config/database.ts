// const database = new
import Database from '../models/database';

const database = new Database({
  host: process.env.DB_HOST || '127.0.0.1',
  port: parseInt(String(process.env.DB_PORT)) || 3306,
  database: process.env.DB_NAME || 'esmart',
  user: process.env.DB_USER || 'root',
  password: process.env.DB_PASS || '',
});

export default database;
