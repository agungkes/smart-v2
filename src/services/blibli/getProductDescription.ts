import getFromApi from './helpers/getFromApi';

const getProductDescription = async (productId: string) => {
  try {
    const res = await getFromApi<BlibliProductDescription>(
      `https://www.blibli.com/backend/product-detail/products/${productId}/description`
    );

    if (res) {
      return res.data.value;
    }

    return '';
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getProductDescription;
