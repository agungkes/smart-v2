interface ProductBlibliSearch {
  code: number;
  status: string;
  data: ProductBlibliData;
}

interface ProductBlibliData {
  pageType: Record<string, unknown>[];
  searchTerm: string;
  suggestions: Record<string, unknown>[];
  correctedSearchResponses: Record<string, unknown>[];
  ageRestricted: boolean;
  products: Product[];
  sorting: Sorting;
  filters: Record<string, unknown>[];
  quickFilters: Record<string, unknown>[];
  paging: Paging;
  maxOffers: number;
  serverCurrentTime: number;
  productInfo: {[key: string]: ProductInfo};
  code: string;
  selectedCategoryIds: Record<string, unknown>[];
  exclusiveCampProducts: Record<string, unknown>[];
  exclusiveCampResponse: ExclusiveCampResponse;
  responseFlags: Record<string, unknown>[];
  showRestrictedMsg: boolean;
  redirectionUrl: string;
  trackerFields: TrackerFields;
  productsPerRow: number;
  needMoreSearchResponse: boolean;
  intentAttributes: Record<string, unknown>;
  nerAttributes: Record<string, unknown>;
  intentApplied: boolean;
  nerApplied: boolean;
  showAllCategories: boolean;
  searchPageUrl: string;
  catIntentFailed: boolean;
  sellerAdsPosition: number[];
  sponsorProducts: SponsorProduct[];
  relatedSearchTermsPosition: number;
  nearbyLocationFailed: boolean;
  correctedNearbyLocation: string;
  interspersedCardFilters: Record<string, unknown>[];
}

interface ExclusiveCampResponse {
  exclusiveCampRow: number;
  promoBgImage: string;
  promoTitleImage: string;
  campaignCode: string;
}

interface Paging {
  page: number;
  total_page: number;
  item_per_page: number;
  total_item: number;
}

interface ProductInfo {
  tags: Tag[];
}

enum Tag {
  BigProduct = 'BIG_PRODUCT',
  MerchantVoucher = 'MERCHANT_VOUCHER',
  Pristine = 'PRISTINE',
  Regular = 'REGULAR',
  ZeroPercentInstallment = 'ZERO_PERCENT_INSTALLMENT',
}

interface Product {
  id: string;
  sku: string;
  merchantCode: string;
  status: Status;
  name: string;
  price: Price;
  images: string[];
  rootCategory: RootCategory;
  brand: Brand;
  review: Review;
  itemCount: number;
  defaultSku: string;
  itemSku: string;
  tags: Tag[];
  formattedId: string;
  url: string;
  attributes: Attribute[];
  productFeatures: string;
  storeClosingInfo: StoreClosingInfo;
  promoEndTime: number;
  debugData: Record<string, unknown>;
  allCategories: string[];
  merchantVoucherCount: number;
  expandedProducts: ExpandedProduct[];
  location: Location;
  numLocations: number;
  badge: Badge;
  size?: string[];
  level0Id: string;
  uniqueSellingPoint: string;
  isCheap: boolean;
  official: boolean;
  soldRangeCount?: SoldRangeCount;
}

interface Attribute {
  optionListingType?: OptionListingType;
  values?: Value[];
  count: number;
}

enum OptionListingType {
  ColorPalete = 'COLOR_PALETE',
}

enum Value {
  Multicolor = 'MULTICOLOR',
}

interface Badge {
  merchantBadge: MerchantBadge;
  merchantBadgeUrl: string;
}

enum MerchantBadge {
  Bronze = 'Bronze',
  None = 'None',
}

enum Brand {
  SepatuJohnson = 'Sepatu Johnson',
  SepatuKultur = 'Sepatu Kultur',
  SepatuTrendi = 'Sepatu Trendi',
}

interface ExpandedProduct {
  status: Status;
  price: Price;
  images: string[];
  review: Review;
  itemCount: number;
  defaultSku: string;
  tags: Tag[];
  url: string;
  promoEndTime: number;
  merchantVoucherCount: number;
  numLocations: number;
  badge: Badge;
  size: string[];
  official: boolean;
}

interface Price {
  priceDisplay: string;
  strikeThroughPriceDisplay?: string;
  discount: number;
  minPrice: number;
  offerPriceDisplay?: string;
}

interface Review {
  rating: number;
  count: number;
  absoluteRating: number;
}

enum Status {
  Available = 'AVAILABLE',
}

enum Location {
  Bandung = 'Bandung',
  Bekasi = 'Bekasi',
  Denpasar = 'Denpasar',
  Jakarta = 'Jakarta',
  Karawang = 'Karawang',
  Tegal = 'Tegal',
}

interface RootCategory {
  id: string;
  name: Name;
}

enum Name {
  BlibliHasanah = 'Blibli Hasanah',
  FashionPria = 'Fashion Pria',
  FashionWanita = 'Fashion Wanita',
  GaleriIndonesia = 'Galeri Indonesia',
  IbuAnak = 'Ibu & Anak',
}

interface SoldRangeCount {
  en: string;
  id: string;
}

interface StoreClosingInfo {
  storeClosed: boolean;
  endDate: number;
  delayShipping: boolean;
}

interface Sorting {
  parameter: string;
  options: Option[];
}

interface Option {
  label: string;
  name: string;
  value: number;
  selected: boolean;
}

interface SponsorProduct {
  destinationUrl: string;
  imageUrl: string;
  mrp: number;
  name: string;
  rank: number;
  salePrice: number;
  sclid: string;
  score: number;
  sellerId: {[key: string]: string};
  skuId: string;
  uclid: string;
}

enum SellerID {
  Kel70014 = 'KEL-70014',
  Lau70021 = 'LAU-70021',
  Sto60038 = 'STO-60038',
  We070001 = 'WE0-70001',
}

interface TrackerFields {
  group_type: string;
}

interface BlibliShop {
  code: number;
  status: string;
  data: BlibliShopData;
}

interface BlibliShopData {
  pageType: Record<string, unknown>[];
  ageRestricted: boolean;
  products: Record<string, unknown>[];
  sorting: Sorting;
  filters: BlibliShopFilter[];
  quickFilters: QuickFilter[];
  paging: Paging;
  maxOffers: number;
  serverCurrentTime: number;
  productInfo: Record<string, unknown>;
  code: string;
  selectedCategoryIds: Record<string, unknown>[];
  visibleCategoryIds: Record<string, unknown>[];
  exclusiveCampProducts: Record<string, unknown>[];
  responseFlags: Record<string, unknown>[];
  showRestrictedMsg: boolean;
  redirectionUrl: string;
  trackerFields: Record<string, unknown>;
  productsPerRow: number;
  needMoreSearchResponse: boolean;
  merchant: Merchant;
  customCatalogs: string[];
}

interface BlibliShopFilter {
  name: string;
  type: string;
  searchable: boolean;
  parameter: string;
  theme: string;
  data: FilterDatum[];
  horizontal: boolean;
  label?: string;
  parentFacets?: Record<string, unknown>[];
  sublist?: Sublist[];
}

interface FilterDatum {
  label: string;
  value: string;
  selected: boolean;
  id?: string;
  level?: number;
  subCategory?: SubCategory[];
  subCategorySelected?: boolean;
  restrictedCategory?: boolean;
  code?: string;
  query?: string;
  count?: number;
}

interface SubCategory {
  label: string;
  value: string;
  selected: boolean;
  id: string;
  level: number;
  subCategory: SubCategory[];
  subCategorySelected: boolean;
  restrictedCategory: boolean;
}

interface Sublist {
  title: string;
  parameter: string;
  data: SublistDatum[];
}

interface SublistDatum {
  label: string;
  value: string;
  selected: boolean;
}

interface Merchant {
  activationDate: number;
  description: string;
  name: string;
  level: string;
  logo: string;
  productQualityRating: number;
  official: boolean;
  cncCatalogBanner: boolean;
  orderFullfilmentRating: number;
  province: string;
  respondTimeRating: number;
  url: string;
  badge: Badge;
  type: string;
  hasLayout: boolean;
}

interface Badge {
  id: string;
  rating: number;
  name: string;
  imageUrl: string;
  orderCompletedRating: number;
  deliveryRating: number;
  productPerformanceRating: number;
  calculationDays: number;
  fastCustomerResponseRating: number;
  fastFulfillmentRating: number;
  perfectTransactionRating: number;
  positiveFeedbackRating: number;
  tooltipUrl: TooltipURL;
}

interface TooltipURL {
  rating: string;
  badge: string;
}

interface Paging {
  page: number;
  total_page: number;
  item_per_page: number;
  total_item: number;
}

interface QuickFilter {
  name: string;
  label: string;
  type: string;
  searchable: boolean;
  parameter: string;
  theme: string;
  data: QuickFilterDatum[];
  horizontal: boolean;
}

interface QuickFilterDatum {
  label: string;
  value: string;
  count?: number;
  selected: boolean;
  parameter?: string;
}

interface Sorting {
  parameter: string;
  options: Option[];
}

interface Option {
  label: string;
  name: string;
  value: number;
  selected: boolean;
}

interface BlibliProductDescription {
  code: number;
  status: string;
  data: BlibliProductDescriptionData;
}

interface BlibliProductDescriptionData {
  value: string;
}
