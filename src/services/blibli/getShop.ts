import dayjs from 'dayjs';
import {stringify} from 'querystring';
import getFromApi from './helpers/getFromApi';

const DAY_FORMAT = 'YYYY-MM-DD HH:mm:ss';
const getShop = async (shopId: string) => {
  try {
    console.log(`Sedang mengambil detail toko [${shopId}]`);

    const query = stringify({
      excludeProductList: true,
      promoTab: true,
      multiCategory: true,
      facetOnly: true,
    });
    const res = await getFromApi<BlibliShop>(
      `https://www.blibli.com/backend/search/merchant/${shopId}?${query}`
    );

    if (res) {
      const merchantInfo = res.data.merchant;

      return {
        name: merchantInfo.name,
        location: merchantInfo.province,
        url: merchantInfo.url,
        description: merchantInfo.description,
        joined_since: dayjs(merchantInfo.activationDate).format(DAY_FORMAT),
        seller_rate: merchantInfo.productQualityRating,
        total_product: res.data.paging.total_item,
      };
    }

    return {};
  } catch (error) {
    console.log('[getShop]: ', error.message);
    throw new Error(error.message);
  }
};

export default getShop;
