import {stringify} from 'querystring';
import dayjs from 'dayjs';
import getShop from './getShop';
import getProductDescription from './getProductDescription';
import {URL} from 'url';
import pMap from 'p-map';
import {Job} from 'bullmq';
import getFromApi from './helpers/getFromApi';
import database from '../../config/database';
import puppeteerPool from '../../lib/puppeteerPool';
import autoScroll from '../../lib/autoScroll';
import screenshot from '../../helpers/screenshot';
import uploadImage from '../../helpers/uploadImage';
import queue from '../../lib/queue';

const getFromPage = async ({data}: Job<JobData>) => {
  console.log(
    `[blibli] Sedang mengambil data pada halaman ${
      Number(data.page) + 1
    } dengan kata kunci ${data.query}`
  );

  const query = stringify({
    sort: 0,
    page: Number(data.page) + 1,
    start: Number(data.page) * 40,
    searchTerm: data.query,
    intent: false,
    merchantSearch: false,
    multiCategory: true,
    channelId: 'web',
    showFacet: false,
    location: data.location.id,
  });

  try {
    const res = await getFromApi<ProductBlibliSearch>(
      `https://www.blibli.com/backend/search/products?${query}`
    );

    if (res) {
      const products = Array.from(new Set([...res.data.products]));
      const totalProducts = res.data.paging.total_item;
      await database.updateProductTotal(Number(data.id_keyword), totalProducts);

      await pMap(
        products,
        async product => {
          const browser = await puppeteerPool.acquire();
          const page = await browser.newPage();

          try {
            const start_time = process.hrtime();
            const shop = await getShop(product.merchantCode);

            if (shop) {
              const description = await getProductDescription(product.itemSku);

              const end_time = process.hrtime(start_time);
              const processing_time =
                ((end_time[0] * 1e9 + end_time[1]) * 1e-6) / 100;

              const url = new URL(`https://blibli.com${product.url}`);

              const productId = await database.save({
                created_by: Number(data.created_by),
                url: `https://blibli.com${url.pathname}`,
                store: {
                  seller_rate: shop.seller_rate,
                  id_marketplace: Number(data.id_marketplace),
                  url: shop.url,
                  name: shop.name,
                  location: shop.location,
                  description: shop.description,
                  joined_since: shop.joined_since,
                  followers: 0,
                },
                product: {
                  id_keywords: Number(data.id_keyword),
                  id_commodity: Number(data.id_commodity),
                  name: product.name,
                  description: description,
                  category: product.rootCategory.name,
                  category_url: '',
                  picture: product.images[0],
                  url: `https://blibli.com${url.pathname}`,
                },
                data_product: {
                  price: product.price.minPrice,
                  crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                  rating: product.review.rating,
                  discount: product.price.discount,
                  item_count: product.itemCount,
                  sold_count: 0,
                  view_count: 0,
                  favorited_count: 0,
                  item_condition: 'baru',
                  processing_time: String(processing_time),
                },
              });

              await page.goto(url.origin + url.pathname, {
                waitUntil: 'networkidle2',
              });
              await autoScroll(page);
              const image = await screenshot(page, url.origin + url.pathname);
              await uploadImage(image, productId);
            }
          } catch (error) {
            throw new Error(error.message);
          } finally {
            await page.close();
            await puppeteerPool.release(browser);
          }
        },
        {
          concurrency: 2,
        }
      );

      // Add screenshot as bulk

      const totalPages = res.data.paging.total_page;
      if (
        Number(data.page) + 1 <= totalPages &&
        Number(data.page) + 1 <= 100 &&
        data.firstPage
      ) {
        console.log(
          'MENUJU HALAMAN SELANJUTNYA -> ',
          Number(data.page) + 1,
          ' dari total halaman ',
          totalPages
        );

        for (let index = 0; index < totalPages; index++) {
          await queue
            .getQueue(data.query, 'blibli')
            .add(`blibli:${data.location.id}${data.query}:page-${index}`, {
              ...data,
              page: index,
              firstPage: false,
            });
        }
      }
    }

    return Promise.resolve();
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getFromPage;
