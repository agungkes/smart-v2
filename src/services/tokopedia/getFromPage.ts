import dayjs from 'dayjs';
import getProductDetail from './getProductDetail';
import {stringify} from 'querystring';
import {performance, PerformanceObserver} from 'perf_hooks';
import pMap from 'p-map';
import {Job} from 'bullmq';
import getFromGraph from './helpers/getFromGraph';
import {SEARCH} from './constant';
import url from '../../helpers/url';
import database from '../../config/database';
import puppeteerPool from '../../lib/puppeteerPool';
import autoScroll from '../../lib/autoScroll';
import screenshot from '../../helpers/screenshot';
import uploadImage from '../../helpers/uploadImage';
import queue from '../../lib/queue';

// let filterIndex = 0;

let processing_time = 0;
const observer = new PerformanceObserver(list =>
  list.getEntries().forEach(entry => {
    if (entry.name === 'getProductDetail') {
      processing_time = entry.duration;
    }
  })
);
observer.observe({buffered: true, entryTypes: ['function']});

const getFromPage = async ({data}: Job<JobData>) => {
  try {
    console.log(
      `[tokopedia] Melakukan pencarian pada halaman ${
        Number(data.page) + 1
      } dengan kata kunci ${data.query}`
    );

    const variableParams = {
      q: data.query,
      scheme: 'http',
      device: 'desktop',
      st: 'product',
      source: 'search',
      page: Number(data.page) + 1,
      start: Number(data.page) * 60,
      rows: (Number(data.page) + 1) * 60,
      fcity: encodeURIComponent(data.location.id),
    };

    const res = await getFromGraph<TokopediaSearch>({
      operationName: 'SearchProductQueryV4',
      query: SEARCH,
      variables: {
        params: stringify(variableParams),
      },
    });

    if (res) {
      const totalProducts = res.data.ace_search_product_v4.header.totalData;
      const totalPages = Math.ceil(totalProducts / 60);
      const products = res.data.ace_search_product_v4.data.products.map(e => ({
        ...e,
        url: url(e.url),
      }));

      await database.updateProductTotal(Number(data.id_keyword), totalProducts);

      await pMap(
        products,
        async product => {
          const browser = await puppeteerPool.acquire();
          const page = await browser.newPage();

          try {
            const shopDomain = product.shop.url.replace(/[^\\]+\//g, '');
            const productKey = product.url.replace(/[^\\]+\//g, '');
            console.log(`Mengambil data produk ${productKey}`);

            // Measure performance
            const wrapped = performance.timerify(getProductDetail);
            const productDetail = await wrapped({
              productKey,
              shopDomain,
            });

            if (productDetail) {
              const productId = await database.save({
                store: {
                  id_marketplace: Number(data.id_marketplace),
                  url: `https://tokopedia.com/${shopDomain}`,
                  name: product.shop.name,
                  location: product.shop.city,
                },
                product: {
                  id_keywords: Number(data.id_keyword),
                  id_commodity: Number(data.id_commodity),
                  name: productDetail.name,
                  description: productDetail.description,
                  category: productDetail.category,
                  category_url: productDetail.category_url,
                  picture: productDetail.picture,
                  url: productDetail.url,
                },
                data_product: {
                  price: productDetail.price.toString(),
                  crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                  rating: productDetail.rating,
                  discount: productDetail.discount,
                  item_count: Number(productDetail.item_count),
                  sold_count: Number(productDetail.sold_count),
                  view_count: Number(productDetail.view_count),
                  favorited_count: 0,
                  processing_time: String(processing_time),
                  item_condition: productDetail.condition,
                },
                url: productDetail.url,
                created_by: Number(data.created_by),
              });

              await page.goto(product.url, {
                waitUntil: 'networkidle2',
                timeout: 60000,
              });
              await autoScroll(page);
              const image = await screenshot(page, product.url);

              await uploadImage(image, productId);
            }
          } catch (error) {
            throw new Error(error.message);
          } finally {
            await page.close();
            await puppeteerPool.release(browser);
          }
        },
        {
          concurrency: 2,
        }
      );

      // Add screenshot as bulk

      // create random delay from 10-30s

      if (
        Number(data.page) + 1 <= totalPages &&
        Number(data.page) + 1 < 82 &&
        data.firstPage
      ) {
        console.log(
          'MENUJU HALAMAN SELANJUTNYA -> ',
          Number(data.page) + 1,
          ' dari total halaman ',
          totalPages
        );

        for (
          let index = Number(data.page) + 1;
          index < (totalPages > 82 ? 82 : totalPages);
          index++
        ) {
          await queue
            .getQueue(data.query, 'tokopedia')
            .add(`tokopedia:${data.location.id}${data.query}:page-${index}`, {
              ...data,
              page: index,
              firstPage: false,
            });
        }
      } else {
        // Jika sudah melebihi dari 81 halaman
        // akan ditambahkan pencarian dengan filter baru
        // dan akan memanggil fungsi search lagi dengan parameter yang berbeda
        // currentPage = 0;
        // filterIndex++;
        // const filter = await getFilterSearch({
        //   page: variableParams.page,
        //   q: variableParams.q,
        //   source: variableParams.source,
        //   st: variableParams.st,
        // });
        // console.log(
        //   `Sudah melebihi 82 halaman, masih terdapat produk yang lain reset nomor halaman ${currentPage} ${
        //     filterIndex - 1
        //   } dengan penambahan filter ${JSON.stringify({
        //     [filter[filterIndex - 1].key]: filter[filterIndex - 1].value,
        //   })}`
        // );
        // if (filterIndex === filter.length - 1) {
        //   observer.disconnect();
        //   if (parentPort) {
        //     parentPort.postMessage({
        //       done: true,
        //       exit: true,
        //     });
        //   }
        //   return;
        // }
        // await search({
        //   keyword,
        //   page: currentPage,
        //   [filter[filterIndex - 1].key]: encodeURIComponent(
        //     filter[filterIndex - 1].value
        //   ),
        // });
      }
      return;
    }
  } catch (error) {
    console.log(error);
  }
};

export default getFromPage;
