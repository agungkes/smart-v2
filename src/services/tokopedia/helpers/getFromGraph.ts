import NodeCache from 'node-cache';
import crypto from 'crypto';
import getProxy from '../../../helpers/getProxy';

type GetFromGraph = {
  operationName: string;
  query: string;
  variables: {
    [key: string]: string | null | number;
  };
  extraHeaders?: string[];
};

const GraphCache = new NodeCache({
  stdTTL: 3600 * 24, // 1 hari
});

const getFromGraph = async <T = unknown>(param: GetFromGraph): Promise<T> => {
  try {
    const cacheKey = crypto
      .createHash('md5')
      .update(JSON.stringify(param))
      .digest('hex');

    const isAvailableInCache: string | undefined = GraphCache.get(cacheKey);
    if (!isAvailableInCache) {
      const {extraHeaders, ...restParam} = param;
      const proxy = getProxy();

      const {curly} = await import('node-libcurl');
      const {statusCode, data} = await curly.post(
        'https://gql.tokopedia.com/',
        {
          httpHeader: [
            'Content-Type: application/json',
            'x-device: desktop',
            'x-source: tokopedia-lite',
            'x-tkpd-lite-service: zeus',
            'origin: https://www.tokopedia.com',
            'Accept: application/json',
            ...(extraHeaders || []),
          ],
          postFields: JSON.stringify(restParam),
          httpProxyTunnel: proxy,
        }
      );

      if (statusCode === 200) {
        GraphCache.set(cacheKey, JSON.stringify(data));
        return data;
      }
      throw new Error('Terjadi kesalahan! Status kode tidak 200');
    } else {
      return JSON.parse(isAvailableInCache);
    }
  } catch (error) {
    console.log(`[getFromGraph]: ${error.message}`);
    throw new Error(error.message);
  }
};

export default getFromGraph;
