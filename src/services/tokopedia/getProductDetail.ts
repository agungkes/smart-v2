import dayjs from 'dayjs';
import {PRODUCT} from './constant';
import getFromGraph from './helpers/getFromGraph';
type GetProductDetail = {
  shopDomain: string;
  productKey: string;
};

type ComponentKey = 'product_media' | 'product_content' | 'product_detail';
const getComponent =
  (component: ProductComponent[]) => (name: ComponentKey) => {
    const comp = (component || [])
      .filter(f => {
        return ['product_media', 'product_content', 'product_detail'].includes(
          f.name
        );
      })
      .find(f => f.name === name);

    if (comp) {
      return comp.data[0];
    }

    throw new Error(`Tidak ditemukan key dengan nama: ${name}`);
  };

const getProductDetail = async (params: GetProductDetail) => {
  try {
    const res: Product = await getFromGraph({
      operationName: 'PDPGetLayoutQuery',
      query: PRODUCT,
      variables: {...params, apiVersion: 1},
      extraHeaders: ['x-tkpd-akamai: pdpGetLayout', 'x-version: d11dd64'],
    });

    if (res) {
      const data = res.data.pdpGetLayout;

      /**
       * TODO:
       * + Memasukkan produk yang null ke dalam log untuk di analisis lebih lanjut
       */
      // if (data === null) {
      //   console.log('NULL BOSQUE: ', params);
      // }
      if (data) {
        const components = getComponent(data.components ?? []);
        const productMedia = components('product_media');
        const productContent = components('product_content');
        const productDetail = components('product_detail');

        const productDetailContent = productDetail.content!.find(
          f => f.title === 'Deskripsi'
        );
        const description = productDetailContent
          ? productDetailContent.subtitle
          : '';

        return {
          name: productContent.name,
          price: productContent.price!.value ?? 0,
          crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
          rating: data.basicInfo.stats.rating ?? 0,
          discount: productContent.campaign!.percentageAmount ?? 0,
          category: data.basicInfo.category.name,
          category_url: data.basicInfo.category.breadcrumbURL,
          picture: productMedia.media![0].urlOriginal,
          screenshot: '',
          note: '',
          url: `https://tokopedia.com/${params.shopDomain}/${data.basicInfo.alias}`,
          weight: data.basicInfo.weight,
          condition: data.basicInfo.condition === 'NEW' ? 'baru' : 'bekas',
          status: data.basicInfo.status,
          item_count: productContent.stock!.value,
          sold_count: data.basicInfo.txStats.countSold ?? 0,
          view_count: data.basicInfo.stats.countView ?? 0,
          review_count: data.basicInfo.stats.countReview ?? 0,
          discussion_count: data.basicInfo.stats.countTalk ?? 0,
          description: description,
        };
      }
    }

    return Promise.resolve();
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getProductDetail;
