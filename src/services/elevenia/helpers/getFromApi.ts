import NodeCache from 'node-cache';
import crypto from 'crypto';
import getProxy from '../../../helpers/getProxy';

const GraphCache = new NodeCache({
  stdTTL: 3600 * 24, // 1 hari
});

const getFromApi = async <T = unknown>(url: string): Promise<T> => {
  try {
    const cacheKey = crypto
      .createHash('md5')
      .update(JSON.stringify(url))
      .digest('hex');

    const isAvailableInCache: string | undefined = GraphCache.get(cacheKey);
    if (!isAvailableInCache) {
      const proxy = getProxy();

      const {curly} = await import('node-libcurl');
      const {statusCode, data} = await curly.get(url, {
        httpHeader: ['x-requested-with: XMLHttpRequest'],
        httpProxyTunnel: proxy,
      });

      if (statusCode === 200) {
        GraphCache.set(cacheKey, JSON.stringify(data));
        return data;
      }
      console.log(data, statusCode);

      throw new Error('Terjadi kesalahan! Status kode tidak 200');
    } else {
      return JSON.parse(isAvailableInCache);
    }
  } catch (error) {
    console.log(`[getFromApi]: ${error.message}`);
    throw new Error(error.message);
  }
};

export default getFromApi;
