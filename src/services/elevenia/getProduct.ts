import dayjs from 'dayjs';
import {parseHTML} from 'linkedom';
import type {Node} from 'linkedom/types/interface/node';
import getFromApi from './helpers/getFromApi';

const getContent = (document: any) => (key: string, asNumber?: boolean) => {
  const elem = document.querySelector(key) as Node;

  if (elem) {
    const content = elem.textContent;
    if (asNumber) {
      return Number(String(content).replace(/\D/g, ''));
    }
    return content;
  }

  return '';
};

const getProduct = async (productUrl: string) => {
  // const product = productUrl.replace(/.+?(?=\/\/).+\//g, '')
  // const productId = productUrl.replace(/.+-/g, '');
  try {
    const res = await getFromApi<any>(productUrl);
    if (res) {
      const {document} = parseHTML(res);
      const content = getContent(document);

      const imageElement = document.querySelector('.big');
      const image = (imageElement && imageElement.getAttribute('src')) || '';

      const priceElement = document.querySelector('.price');
      const normalPriceElement = document.querySelector('.normPrice');
      const price =
        (priceElement && priceElement?.textContent?.replace(/\D/g, '')) || 0;
      const normalPrice =
        (normalPriceElement &&
          normalPriceElement.textContent?.replace(/\D/g, '')) ||
        0;

      let discount = '0';
      if (String(price) !== '0' && normalPrice > 0) {
        discount = ((Number(price) / Number(normalPrice)) * 100).toFixed(2);
      }

      const categories = Array.from(document.querySelectorAll('.cate'));
      const category = categories
        .map(e => (e && e.getAttribute('title')) || '')
        .join('|');
      const category_url = categories
        .map(
          e => (e && `https://elevenia.co.id${e.getAttribute('href')}`) || ''
        )
        .join('|');

      const storeImageElement = document.querySelector('.storeImg');
      const storeUrl =
        (storeImageElement && storeImageElement.getAttribute('href')) || '';

      let description = '';
      const frameDescription = content('#prdDescIfrm');
      if (frameDescription) {
        const contentFrame = frameDescription.contentDocument;
        if (contentFrame) {
          const descSelector = contentFrame.querySelector(
            '#productDescContent'
          );
          if (descSelector) {
            description = descSelector.textContent;
          }
        }
      }

      return {
        name: content('h1'),
        price: price,
        crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
        discount: discount ?? 0,
        category,
        category_url,
        picture: `${image}`,
        condition: 'baru',
        item_count: 0,
        sold_count: 0,
        view_count: 0,
        favorited_count: 0,
        rating_count: 0,
        description,
        shop: {
          name: content('.storeNm'),
          location: content('.placeStore'),
          url: `https://elevenia.co.id${storeUrl}`,
        },
      };
    }
    throw new Error('Tidak berhasil mendapatkan detail product');
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getProduct;
