interface Search {
  template: string;
  totalCount: number;
  viewType: string;
  pageNum: number;
}
