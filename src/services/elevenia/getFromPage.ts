import {stringify} from 'querystring';
import {parseHTML} from 'linkedom';
import dayjs from 'dayjs';
import {performance, PerformanceObserver} from 'perf_hooks';
import pMap from 'p-map';
import {Job} from 'bullmq';
import getFromApi from './helpers/getFromApi';
import {SEARCH_API} from './constant';
import database from '../../config/database';
import puppeteerPool from '../../lib/puppeteerPool';
import getProduct from './getProduct';
import autoScroll from '../../lib/autoScroll';
import screenshot from '../../helpers/screenshot';
import uploadImage from '../../helpers/uploadImage';
import queue from '../../lib/queue';

let processing_time = 0;
const observer = new PerformanceObserver(list =>
  list.getEntries().forEach(entry => {
    if (entry.name === 'getProduct') {
      processing_time = entry.duration;
    }
  })
);

const getFromPage = async ({data}: Job<JobData>) => {
  try {
    const {
      id_marketplace,
      id_commodity,
      created_by,
      id_keyword,
      page,
      firstPage,
    } = data;

    console.log(
      `[elevenia] Melakukan pencarian pada halaman ${
        Number(data.page) + 1
      } dengan kata kunci ${data.query}`
    );

    const query = stringify({
      sortCd: 'NP',
      isBack: 'N',
      gridItemPerRow: '4',
      kwd: data.query,
      pageSize: 60,
      pageNum: Number(data.page) + 1,
      pageType: 'TTS',
      researchFlag: false,
      locParam: data.location.name,
    });

    const res = await getFromApi<Search>(`${SEARCH_API}?${query}`);

    if (res) {
      const templateData = res.template;
      const totalProduct = res.totalCount;
      const totalPages = Math.ceil(Number(totalProduct) / 60);

      await database.updateProductTotal(Number(data.id_keyword), totalPages);

      const {document} = parseHTML(templateData);
      const products = Array.from(document.querySelectorAll('.itemList a'))
        .map(e => (e && e.getAttribute('href')) || '')
        .filter(f => !f.includes('#') && f.includes('prd'))
        .filter((value, index, self) => self.indexOf(value) === index)
        .map(r => r.replace('http', 'https'));
      await pMap(
        products,
        async url => {
          const browser = await puppeteerPool.acquire();
          const page = await browser.newPage();
          const getProductWrapped = performance.timerify(getProduct);
          try {
            const product = await getProductWrapped(url);
            if (product) {
              observer.disconnect();

              const productId = await database.save({
                store: {
                  id_marketplace: Number(id_marketplace),
                  name: product.shop.name,
                  location: product.shop.location,
                  url: product.shop.url,
                },
                product: {
                  id_keywords: Number(id_keyword),
                  id_commodity: Number(id_commodity),
                  name: product.name,
                  description: product.description,
                  category: product.category,
                  category_url: product.category_url,
                  picture: product.picture,
                  url,
                },
                data_product: {
                  crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                  rating: product.rating_count,
                  discount: Number(product.discount),
                  item_count: product.item_count,
                  sold_count: product.sold_count,
                  view_count: product.view_count,
                  favorited_count: 0,
                  processing_time: String(processing_time),
                  item_condition: product.condition,
                },
                url,
                created_by: Number(created_by),
              });

              await page.goto(url, {
                waitUntil: 'networkidle2',
              });
              await autoScroll(page);

              const image = await screenshot(page, url);

              await uploadImage(image, productId);
            }
          } catch (error) {
            throw new Error(error.message);
          } finally {
            await page.close();
            await puppeteerPool.release(browser);
          }
        },
        {
          concurrency: 2,
        }
      );

      if (Number(page) + 1 < totalPages && firstPage) {
        console.log(
          'MENUJU HALAMAN SELANJUTNYA -> ',
          Number(data.page) + 1,
          ' dari total halaman ',
          totalPages
        );

        for (let index = 0; index < totalPages; index++) {
          await queue
            .getQueue(data.query, 'elevenia')
            .add(`elevenia:${data.location.id}${data.query}:page-${index}`, {
              ...data,
              page: index,
              firstPage: false,
            });
        }
      }

      return Promise.resolve();
    }
    throw new Error('Tidak ditemukan produk');
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getFromPage;
