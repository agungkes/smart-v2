import fetch from '@agungkes/fetch';
import {stringify} from 'querystring';
import getProduct from './getProduct';
import dayjs from 'dayjs';
import {performance, PerformanceObserver} from 'perf_hooks';
import pMap from 'p-map';
import {Job} from 'bullmq';
import puppeteerPool from '../../lib/puppeteerPool';
import database from '../../config/database';
import autoScroll from '../../lib/autoScroll';
import screenshot from '../../helpers/screenshot';
import uploadImage from '../../helpers/uploadImage';
import queue from '../../lib/queue';

const currentPage = 1;
let processing_time = 0;
const observer = new PerformanceObserver(list =>
  list.getEntries().forEach(entry => {
    if (entry.name === 'getProductDetail') {
      processing_time = entry.duration;
    }
  })
);
observer.observe({buffered: true, entryTypes: ['function']});

const getFromPage = async ({data}: Job<JobData>) => {
  try {
    const {id_marketplace, location} = data;

    console.log(
      `[jdid] Melakukan pencarian pada halaman ${currentPage} dengan kata kunci ${
        data.query
      } ${(location && 'pada lokasi ' + location.name) || ''}`
    );

    const query = stringify({
      keywords: data.query,
      page: Number(data.page) + 1,
      pagesize: 60,
      sortType: 'sort_default_desc',
      tabFilter: `sender_addr,${location.id};` || '',
    });

    const res = await fetch<JDSearch>(
      `https://color.jd.id/m.jd.id/search/1.0?${query}`,
      {
        headers: {
          authority: 'color.jd.id',
          'x-api-platform': 'PC',
          origin: 'https://www.jd.id',
          'x-api-timestamp': new Date().getTime().toString(),
        },
        proxy: undefined,
        ttl: 1,
      }
    );

    if (res) {
      const paragraphs = res.data.paragraphs;

      const products = paragraphs.map(d => ({
        url: `https://www.jd.id/product/${d.Content.title
          .toLowerCase()
          .replace(/\W/g, ' ')
          .replace(/\s+/g, '-')}_${d.spuid}/${d.skuid}.html`,
        shop: {
          name: d.Content.vendername,
          location: d.Content.venderaddress,
        },
        price: d.Content.price,
        title: d.Content.title,
        skuId: Number(d.skuid),
        rating: Number(d.commentscore),
      }));

      await pMap(
        products,
        async ({
          skuId,
          url: productUrl,
          shop,
          title: productName,
          rating: productRating,
        }) => {
          const browser = await puppeteerPool.acquire();
          const page = await browser.newPage();
          try {
            const getProductWithTimer = performance.timerify(getProduct);
            const product = await getProductWithTimer(skuId);

            if (product) {
              observer.disconnect();

              const productId = await database.save({
                store: {
                  id_marketplace: Number(id_marketplace),
                  url: product.shop.url,
                  name: shop.name,
                  location: shop.location || '',
                },
                product: {
                  id_keywords: Number(data.id_keyword),
                  id_commodity: Number(data.id_commodity),
                  name: productName,
                  description: product.description,
                  category: product.category,
                  category_url: product.category_url,
                  picture: product.picture,
                  url: productUrl,
                },
                data_product: {
                  price: product.price.toString(),
                  crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                  rating: productRating,
                  discount: product.discount,
                  item_count: product.item_count,
                  sold_count: product.sold_count,
                  view_count: product.view_count,
                  favorited_count: 0,
                  processing_time: String(processing_time),
                  item_condition: product.condition,
                },
                url: productUrl,
                created_by: Number(data.created_by),
              });

              await page.goto(productUrl, {
                waitUntil: 'networkidle2',
                timeout: 60000,
              });
              await autoScroll(page);

              const image = await screenshot(page, productUrl);

              await uploadImage(image, productId);
            }
          } catch (error) {
            throw new Error(error.message);
          } finally {
            await page.close();
            await puppeteerPool.release(browser);
          }
        },
        {
          concurrency: 2,
        }
      );

      if (paragraphs.length > 0 && data.firstPage) {
        const productTotal = res.data.head.Summary.ResultCount;
        await database.updateProductTotal(
          Number(data.id_keyword),
          productTotal
        );
        const totalPages = res.data.head.Summary.Page.PageCount;

        console.log(
          'MENUJU HALAMAN SELANJUTNYA -> ',
          Number(data.page) + 1,
          ' dari total halaman ',
          totalPages
        );

        for (let index = Number(data.page) + 1; index < totalPages; index++) {
          await queue
            .getQueue(data.query, 'jdid')
            .add(`jdid:${data.location.id}${data.query}:page-${index}`, {
              ...data,
              page: index,
              firstPage: false,
            });
        }
      }
      return Promise.resolve();
    }
    throw new Error('Tidak dapat menemukan produk');
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getFromPage;
