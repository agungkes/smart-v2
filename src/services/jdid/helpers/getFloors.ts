type Floors = {
  key: string;
  value: Record<string, unknown>;
};

const getFloors =
  (floors: Floors[]) => (key: keyof JDData, defaultValue?: string | number) => {
    const data = floors.find(f => f.key === key);
    if (data) {
      return data.value as unknown as JDData[typeof key];
    }

    return defaultValue || '';
  };

export default getFloors;
