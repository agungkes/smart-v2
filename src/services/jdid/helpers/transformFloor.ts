const transformFloor = (floors: Floor[]) =>
  floors
    .filter(e => e.data)
    .map(e => e.data)
    .map(e => {
      const data = e as JDData;
      return Object.keys(data).map(key => ({
        key,
        value: data[key],
      }));
    })
    .flat(2);

export default transformFloor;
