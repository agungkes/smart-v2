import fetch from '@agungkes/fetch';
import {stringify} from 'querystring';

type GetProductDescription = {
  productId: number;
  warehouseId: number;
};
const getProductDescription = async ({
  productId,
  warehouseId,
}: GetProductDescription) => {
  try {
    const query = stringify({
      skuId: productId,
      wareId: warehouseId,
      venderType: -1,
    });
    const res = await fetch<ProductDescription>(
      `https://color.jd.id/jdid_pc_website/sea_item_ware_description/1.0?${query}`,
      {
        headers: {
          'x-api-timestamp': new Date().getTime().toString(),
          'x-api-platform': 'M',
          'x-api-lang': 'id',
          origin: ' https://m.jd.id',
        },
        proxy: undefined,
      }
    );

    if (res) {
      const data = res.data;

      if (data.description) {
        // di php gunakan rawurldecode
        return encodeURIComponent(data.description);
      }
    }
    return '';
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getProductDescription;
