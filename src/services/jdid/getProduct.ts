import fetch from '@agungkes/fetch';
import dayjs from 'dayjs';
import {stringify} from 'querystring';
import getProductDescription from './getProductDescription';
import getFloors from './helpers/getFloors';
import transformFloor from './helpers/transformFloor';

const getDiscount = (originalPrice: number, salePrice: number) =>
  ((originalPrice - salePrice) / originalPrice) * 100;

const getProduct = async (productId: number) => {
  try {
    const query = stringify({
      body: JSON.stringify({
        apolloId: '2aacec34a317421f8e9d1bc25db7f3bd',
        apolloSecret: '7ba107ed3169407490d18d1a4f7ef67e',
        skuId: productId,
      }),
    });

    const res = await fetch<Product>(
      `https://color.jd.id/soa_h5/id_wareBusiness.style/1.0?${query}`,
      {
        headers: {
          'x-api-timestamp': new Date().getTime().toString(),
          'x-api-platform': 'M',
          'x-api-lang': 'id',
          origin: 'https://m.jd.id',
        },
        proxy: undefined,
      }
    );

    if (res) {
      const others = res.others;
      const floors = transformFloor(res.floors);

      const productFloors = getFloors(floors);
      const product = others.wareBaseInfo;

      const description = await getProductDescription({
        productId: product.skuId,
        warehouseId: product.spuId,
      });

      if (floors) {
        const images = productFloors('imageList');
        const stock = productFloors('stockCount', 0);
        const shop = productFloors('venderVo');
        const shopLocation = productFloors('deliveryAddressVO');

        let category = '';
        let category_url = '';
        if (product.cate1 !== undefined) {
          category += product.cate1.catName + '|';
          category_url = product.cate1.targetUrl;
        }

        if (product.cate2 !== undefined) {
          category += product.cate2.catName + '|';
          category_url = product.cate2.targetUrl;
        }

        if (product.cate3 !== undefined) {
          category += product.cate3.catName;
          category_url = product.cate3.targetUrl;
        }

        return {
          name: product.productName,
          price: productFloors('jdPrice', 0),
          crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
          discount: getDiscount(
            Number(productFloors('originalPrice', 0)),
            Number(productFloors('salePrice'))
          ),
          category: category,
          category_url: `https://www.jd.id${category_url}`,
          picture: images
            ? `https://id.360buyimg.com/Indonesia/s880x0_/${images[0]}.dpg.webp`
            : '',
          weight: product.weight,
          condition: 'baru',
          item_count: stock,
          sold_count: 0,
          view_count: 0,
          favorited_count: 0,
          description,
          shop: {
            url: shop.pcUrl,
            followers: shop.shopFollowCount,
            name: shop.name,
            location: `${shopLocation.countyName}, ${shopLocation.cityName}, ${shopLocation.provinceId}`,
          },
        };
      }

      return false;
    }
    throw new Error('Gagal mendapatkan detail produk');
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getProduct;
