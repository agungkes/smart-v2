interface AccessToken {
  access_token: string;
  created_at: number;
  expires_in: number;
  expires_at: number;
  refresh_token: null;
  scope: string;
  token_type: string;
  user_id: null;
}

interface Search {
  data: Datum[];
  meta: Meta;
}

interface Datum {
  active: boolean;
  assurance: boolean;
  available_countries: Record<string, unknown>[];
  category: Category;
  condition: Condition;
  couriers: string[];
  created_at: string;
  deal: Deal;
  default_catalog: null;
  description: string;
  digital_product: boolean;
  discount_percentage: number;
  discount_subsidy: DiscountSubsidy | null;
  for_sale: boolean;
  id: string;
  images: Images;
  imported: boolean;
  installments: Installment[];
  international_couriers: Record<string, unknown>[];
  max_quantity: number;
  merchant_return_insurance: boolean;
  min_quantity: number;
  name: string;
  original_price: number;
  price: number;
  product_sin: Record<string, unknown>[];
  rating: Rating;
  relisted_at: string;
  rush_delivery: boolean;
  shipping: Shipping;
  sku_id: number;
  sla: Sla;
  special_campaign_id: number;
  specs: Specs;
  state: State;
  state_description: Record<string, unknown>[];
  stats: Stats;
  stock: number;
  store: Store;
  tag_pages: TagPage[];
  updated_at: string;
  url: string;
  video_url: string;
  warranty: Warranty;
  weight: number;
  wholesales: Wholesale[];
  without_shipping: boolean;
}

interface Category {
  id: number;
  name: StructureEnum;
  structure: StructureEnum[];
  url: string;
}

enum StructureEnum {
  Boots = 'Boots',
  FashionPria = 'Fashion Pria',
  Formal = 'Formal',
  Sepatu = 'Sepatu',
  SlipOn = 'Slip On',
  SneakerPria = 'Sneaker Pria',
}

enum Condition {
  Baru = 'Baru',
}

interface Deal {
  applied_date?: string;
  discount_price?: number;
  expired_date?: string;
  original_price?: number;
  percentage?: number;
}

interface DiscountSubsidy {
  amount: number;
  applied_date: string;
  expired_date: string;
  max_purchase: number;
  stock: number;
}

interface Images {
  large_urls: string[];
  small_urls: string[];
}

interface Installment {
  bank_issuer: string;
  bank_issuer_name: string;
  logo_url: string;
  terms: number[];
}

interface Rating {
  average_rate: number;
  user_count: number;
}

interface Shipping {
  force_insurance: boolean;
  free_shipping_coverage: number[];
}

interface Sla {
  type: SlaType;
  value: number;
}

enum SlaType {
  Custom = 'custom',
  Empty = '',
  Normal = 'normal',
  Preorder = 'preorder',
  Sameday = 'sameday',
}

interface Specs {
  Brand: Brand;
  ukuran: string[] | string;
  brand?: string;
}

enum Brand {
  Adidas = 'Adidas',
  Baricco = 'Baricco',
  Brodo = 'Brodo',
  CrocodileIndonesia = 'Crocodile Indonesia',
  Crocs = 'Crocs',
  Empty = '',
  Lainnya = 'Lainnya',
}

enum State {
  Available = 'available',
}

interface Stats {
  interest_count: number;
  sold_count: number;
  view_count: number;
  waiting_payment_count: number;
}

interface Store {
  address: Address;
  alert: string;
  avatar_url: string;
  brand_seller: boolean;
  carriers: string[];
  closing: Closing;
  delivery_time: string;
  description: string;
  first_upload_product_at: string;
  flagship: boolean;
  groups: Record<string, unknown>[];
  header_image: HeaderImage;
  id: number;
  inactivity: Inactivity;
  last_order_schedule: LastOrderSchedule;
  level: Level;
  name: string;
  premium_level: PremiumLevel;
  premium_top_seller: boolean;
  rejection: Rejection;
  reviews: Reviews;
  sla: Sla;
  subscriber_amount: number;
  term_and_condition: string;
  url: string;
}

interface Address {
  city: string;
  province: Province;
}

enum Province {
  Banten = 'Banten',
  DKIJakarta = 'DKI Jakarta',
  JawaBarat = 'Jawa Barat',
  JawaTengah = 'Jawa Tengah',
  JawaTimur = 'Jawa Timur',
}

interface Closing {
  closed: boolean;
}

interface HeaderImage {
  url: string;
}

interface Inactivity {
  inactive: boolean;
  last_appear_at: string;
}

interface LastOrderSchedule {
  friday?: string;
  monday?: string;
  saturday?: string;
  thursday?: string;
  tuesday?: string;
  wednesday?: string;
  sunday?: string;
}

interface Level {
  image_url: string;
  name: LevelName;
}

enum LevelName {
  CalonJuragan = 'Calon Juragan',
  GoodSeller = 'Good Seller',
  Juragan = 'Juragan',
  Pedagang = 'Pedagang',
  PedagangBesar = 'Pedagang Besar',
  RecommendedSeller = 'Recommended Seller',
  TopSeller = 'Top Seller',
}

enum PremiumLevel {
  Platinum = 'platinum',
}

interface Rejection {
  recent_transactions: number;
  rejected: number;
}

interface Reviews {
  negative: number;
  positive: number;
}

interface TagPage {
  name: string;
  url: string;
}

interface Warranty {
  cheapest: boolean;
}

interface Wholesale {
  lower_bound: number;
  price: number;
}

interface Meta {
  blocked_keywords: BlockedKeywords;
  facets: Facet[];
  http_status: number;
  page: number;
  per_page: number;
  position: Position[];
  price_range: PriceRange;
  suggestions: Suggestions;
  total: number;
  total_pages: number;
}

interface BlockedKeywords {
  blocked: boolean;
  keyword: string;
  message: string;
  type: string;
}

interface Facet {
  children: Facet[];
  count: number;
  id: number;
  name: string;
  permalink: string;
}

interface Position {
  id: string;
  type: PositionType;
}

enum PositionType {
  Business = 'business',
  Organic = 'organic',
  Promoted = 'promoted',
}

interface PriceRange {
  max: null;
  min: null;
}

interface Suggestions {
  typed_keyword: string;
  types: Record<string, unknown>[];
}
