import fetch from '@agungkes/fetch';
import {ACCESS_TOKEN_API} from './constant';

const getAccessToken = async () => {
  try {
    const res = await fetch<AccessToken>(ACCESS_TOKEN_API, {
      method: 'POST',
      ttl: 1800, // expired 1/2 jam
      body: JSON.stringify({application_id: 1, authenticity_token: ''}),
    });

    if (res) {
      return res.access_token;
    }
    throw new Error('Gagal mendapatkan access token');
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getAccessToken;
