export const SEARCH_API = 'http://api.bukalapak.com/multistrategy-products';
export const ACCESS_TOKEN_API =
  'https://www.bukalapak.com/westeros_auth_proxies';
export const AUTHORITY_IPS = [
  '35.241.3.2',
  '35.190.127.143',
  '34.102.240.140',
  '34.102.185.220',
  '34.102.168.83',
  '34.102.144.109',
  '34.98.112.148',
  '34.98.95.142',
];

export const PRODUCT_API = 'https://api.bukalapak.com/products';
export const FILTER_CATEGORY_API =
  'https://api.bukalapak.com/personalizations/user-categories';
