import {stringify} from 'querystring';
import dayjs from 'dayjs';

import getAccessToken from './getAccessToken';
import pMap from 'p-map';
import {Job} from 'bullmq';
import getFromApi from './helpers/getFromApi';
import {SEARCH_API} from './constant';
import puppeteerPool from '../../lib/puppeteerPool';
import screenshot from '../../helpers/screenshot';
import database from '../../config/database';
import uploadImage from '../../helpers/uploadImage';
import queue from '../../lib/queue';

const currentPage = 1;

const getFromPage = async ({data}: Job<JobData>) => {
  try {
    const {id_marketplace, location} = data;

    console.log(
      `[bukalapak] Melakukan pencarian pada halaman ${currentPage} dengan kata kunci ${data.query} pada ${location.type}:${location.name}`
    );

    const accessToken = await getAccessToken();

    const query = stringify({
      facet: true,
      limit: 50,
      keywords: data.query,
      offset: (currentPage - 1) * 50,
      page: currentPage,
      access_token: accessToken,
    });

    const res = await getFromApi<Search>(
      `${SEARCH_API}?${`${
        location.type === 'provinces' ? 'provinces' : 'cities[]'
      }=${location.name}`}&${query}`
    );

    if (res && res.data.length > 0) {
      const products = res.data;
      const totalPages = res.meta.total_pages;

      await pMap(
        products,
        async product => {
          const puppeteer = await puppeteerPool.acquire();
          const page = await puppeteer.newPage();
          try {
            await page.goto(product.url, {
              waitUntil: 'networkidle2',
            });
            const screenshotPath = await screenshot(page, product.url, false);

            const startTime = process.hrtime();
            const message = {
              store: {
                id_marketplace: Number(id_marketplace),
                url: product.store.url,
                name: product.store.name,
                location: `${product.store.address.city}, ${product.store.address.province}`,
                description: product.store.description,
                joined_since: dayjs(
                  product.store.first_upload_product_at
                ).format('YYYY-MM-DD HH:mm:ss'),
                last_update: dayjs(
                  product.store.inactivity.last_appear_at
                ).format('YYYY-MM-DD HH:mm:ss'),
                seller_type: product.store.premium_level,
                followers: product.store.subscriber_amount,
              },
              product: {
                id_keywords: Number(data.id_keyword),
                id_commodity: Number(data.id_commodity),
                name: product.name,
                description: product.description,
                category: product.category.name,
                category_url: product.category.url,
                picture: product.images.large_urls[0],
                url: product.url,
              },
              data_product: {
                price: product.price.toString(),
                crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                rating: product.rating.average_rate,
                discount: product.discount_percentage,
                item_count: Number(product.stock),
                sold_count: Number(product.stats.sold_count),
                view_count: Number(product.stats.view_count),
                favorited_count: 0,
                processing_time: '',
                item_condition: product.condition === 'Baru' ? 'baru' : 'bekas',
              },
              url: product.url,
              created_by: Number(data.created_by),
            };

            const endTime = process.hrtime(startTime);
            const processing_time =
              (endTime[0] * 1000000000 + endTime[1]) / 1000000;
            message.data_product.processing_time = processing_time.toString();

            const productId = await database.save(message);

            await uploadImage(screenshotPath, productId);
          } catch (error) {
            throw new Error(error.message);
          } finally {
            await page.close();
            await puppeteerPool.release(puppeteer);
          }
        },
        {
          concurrency: 2,
        }
      );

      if (
        Number(data.page) <= totalPages &&
        Number(data.page) <= 99 &&
        data.firstPage
      ) {
        console.log(
          'MENUJU HALAMAN SELANJUTNYA -> ',
          Number(data.page) + 1,
          ' dari total halaman ',
          totalPages
        );

        for (let index = 0; index < totalPages; index++) {
          await queue
            .getQueue(data.query, 'bukalapak')
            .add(`bukalapak:${data.location.id}${data.query}:page-${index}`, {
              ...data,
              page: index,
              firstPage: false,
            });
        }
      } else {
        // Jika sudah lebih dari 99 halaman
        // akan ditambahkan filter untuk membantu mencari lebih detail
        // currentPage = 1;
        // filterIndex++;
        // const {filter, totalIndex} = await getFilter({
        //   accessToken,
        //   position: filterIndex,
        // });
        // if (filterIndex < totalIndex - 1) {
        //   await getProductFromSearch({
        //     ...params,
        //     keyword,
        //     page: currentPage,
        //     ...filter,
        //   });
        // }
      }
    }
    throw new Error('[bukalapak] tidak dapat melakukan fetch');
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getFromPage;
