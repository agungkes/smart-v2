import {stringify} from 'querystring';
import dayjs from 'dayjs';
import pMap from 'p-map';
import {Job} from 'bullmq';
import getFromApi from './helpers/getFromApi';
import puppeteerPool from '../../lib/puppeteerPool';
import getShop from './getShop';
import buildUrl from './helpers/buildUrl';
import uploadImage from '../../helpers/uploadImage';
import screenshot from '../../helpers/screenshot';
import queue from '../../lib/queue';
import database from '../../config/database';

import {OlxSearch} from './olx.types';
const filterLocationIndex = 0;
const getFromPage = async ({data}: Job<JobData>) => {
  console.log(
    `Sedang mengambil data pada halaman ${
      Number(data.page) + 1
    } dengan kata kunci ${data.query}`
  );

  const query = stringify({
    page: Number(data.page) + 1,
    platform: 'web-desktop',
    query: data.query,
    location: data.location.id,
    facet_limit: 100,
    location_facet_limit: 20,
    sorting: 'desc-creation',
  });

  const res = await getFromApi<OlxSearch>(
    `https://api.olx.co.id/relevance/search?${query}`
  );

  if (res && res.not_empty) {
    const products = res.data;
    await pMap(
      products,
      async product => {
        const browser = await puppeteerPool.acquire();
        const page = await browser.newPage();

        const start_time = process.hrtime();
        const shop = await getShop(product.user_id);

        if (shop) {
          const end_time = process.hrtime(start_time);
          const processing_time =
            ((end_time[0] * 1e9 + end_time[1]) * 1e-6) / 100;
          const location = product.locations_resolved;
          const url = buildUrl(product.title, product.id);
          try {
            const productId = await database.save({
              created_by: Number(data.created_by),
              url: url,
              store: {
                seller_rate: shop.seller_rate,
                id_marketplace: Number(data.id_marketplace),
                url: shop.url,
                name: shop.name,
                location: `${location.SUBLOCALITY_LEVEL_1_name}, ${location.ADMIN_LEVEL_3_name}, ${location.ADMIN_LEVEL_1_name}, ${location.COUNTRY_name}`,
                description: shop.description,
                joined_since: shop.joined_since,
                followers: 0,
              },
              product: {
                id_keywords: Number(data.id_keyword),
                id_commodity: Number(data.id_commodity),
                name: product.title,
                description: product.description,
                category: '',
                category_url: '',
                picture: product.images[0].full.url,
                url: url,
              },
              data_product: {
                price:
                  (product.price &&
                    product.price.value.display.replace(/\D/g, '')) ||
                  0,
                crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                rating: 0,
                discount: 0,
                item_count: 1,
                sold_count: 0,
                view_count: 0,
                favorited_count: 0,
                item_condition: 'baru',
                processing_time: String(processing_time),
              },
            });

            await page.goto(url);
            const image = await screenshot(page, url, false);
            await uploadImage(image, productId);
          } catch (error) {
            throw new Error(error.message);
          } finally {
            await page.close();
            await puppeteerPool.release(browser);
          }
        }
      },
      {
        concurrency: 2,
      }
    );
    console.log(
      Number(data.page) + 1 <= res.metadata.total_pages &&
        Number(data.page) + 1 <= 49 &&
        data.firstPage,
      data.page,
      res.metadata.total_pages,
      data.firstPage
    );

    if (
      Number(data.page) + 1 <= res.metadata.total_pages &&
      Number(data.page) + 1 <= 49 &&
      data.firstPage
    ) {
      //   await getFromSearch(params);
      console.log(
        'MENUJU HALAMAN SELANJUTNYA -> ',
        Number(data.page) + 1,
        ' dari total halaman ',
        res.metadata.total_pages
      );

      for (
        let index = Number(data.page) + 1;
        index < res.metadata.total_pages;
        index++
      ) {
        await queue
          .getQueue(data.query, 'olx')
          .add(`olx:${data.location.id}${data.query}:page-${index}`, {
            ...data,
            page: index,
            firstPage: false,
          });
      }
    } else if (filterLocationIndex !== Location.length - 1 && !location) {
      //   currentPage = 0;
      //   filterLocationIndex++;
      //   await getFromSearch({
      //     ...params,
      //     location: Location[filterLocationIndex - 1].id,
      //   });
    } else {
      // berhenti
      await database.updateKeywordStatus(Number(data.id_keyword), '0');
    }
  }

  return Promise.resolve();
};

export default getFromPage;
