import dayjs from 'dayjs';
import getFromApi from './helpers/getFromApi';
import {Shop} from './shop.types';

const DAY_FORMAT = 'YYYY-MM-DD HH:mm:ss';
const getShop = async (shopId: string) => {
  try {
    console.log(`Sedang mengambil detail toko [${shopId}]`);

    const res = await getFromApi<Shop>(
      `https://www.olx.co.id/api/v2/users/${shopId}/items?limit=1&status=ACTIVE`
    );
    if (res) {
      const merchantInfo = res.metadata.users[shopId];
      if (merchantInfo) {
        return {
          name: merchantInfo.name ?? '-',
          url: `https://www.olx.co.id/profile/${shopId}`,
          description: merchantInfo.about,
          joined_since: dayjs(merchantInfo.created_at).format(DAY_FORMAT),
          seller_rate: 0,
          total_product: res.metadata.total,
        };
      }
    }
    return {
      name: '',
      url: '',
      description: '',
      joined_since: '',
      seller_rate: 0,
      total_product: 0,
    };
  } catch (error) {
    console.log('[getShop]: ', error.message);
    throw new Error(error.message);
  }
};

export default getShop;
