const buildUrl = (title: string, id: string) =>
  `https://www.olx.co.id/item/${title
    .toLowerCase()
    .replace(/\s/g, '-')
    .replace(/[^\w\s!-]/gi, '')}--iid-${id}`;

export default buildUrl;
