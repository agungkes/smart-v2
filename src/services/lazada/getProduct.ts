import {Page} from 'puppeteer';
import autoScroll from '../../lib/autoScroll';
import captchaSolver from './helpers/captchaSolver';

const getProduct = async (page: Page) => {
  try {
    await captchaSolver(page);
    await autoScroll(page);
    const product = await page.evaluate(() => {
      const getContent =
        (document: Document) => (key: string, attribute?: string) => {
          const selectedElement = document.querySelector(key);
          if (selectedElement) {
            if (attribute) {
              return selectedElement.getAttribute(attribute) || '';
            }
            return selectedElement.textContent ?? '';
          }

          return '';
        };
      const content = getContent(document);

      const category = Array.from(document.querySelectorAll('.breadcrumb_item'))
        .slice(0, -1)
        .map(e => {
          if (e) {
            return e.textContent && e.textContent.trim();
          }

          return '';
        })
        .join('|');
      return {
        name: content('.pdp-mod-product-badge-title'),
        price: content('.pdp-price').replace(/\D/g, ''),
        discount:
          content('.pdp-product-price__discount').replace(/\D/g, '') || 0,
        category,
        category_url: content('.breadcrumb_item:nth-last-child(2) a', 'href'),
        image: `https:${content('.pdp-mod-common-image', 'src')}`,
        weight: 0,
        condition: 'baru',
        item_count: 0,
        sold_count: 0,
        view_count: 0,
        favorited_count: 0,
        rating: content('.score-average'),
        description: content('#module_product_detail .pdp-product-detail'),
        shop: {
          name: content('.seller-name__detail a'),
          followers: 0,
          url: `https:${content('.seller-link a', 'href')}`,
          location: '',
        },
      };
    });
    return product;
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getProduct;
