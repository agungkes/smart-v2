import {Job} from 'bullmq';
import {stringify} from 'querystring';
import puppeteerPool from '../../lib/puppeteerPool';
import getProductFromPage from './getProductFromPage';

const getFromPage = async ({data}: Job<JobData>) => {
  const browser = await puppeteerPool.acquire();
  const page = await browser.newPage();

  try {
    const query = stringify({
      _keyori: 'ss',
      from: 'input',
      page: Number(data.page) + 1,
      q: data.query.replace(/\s/g, '+'),
      location: data.location.id,
    });
    console.log(
      '[lazada] Sedang mengambil pada halaman ',
      Number(data.page) + 1
    );

    await page.goto(`https://www.lazada.co.id/catalog/?${query}`, {
      waitUntil: 'networkidle2',
      timeout: 60000,
    });

    await getProductFromPage({
      browser,
      page,
      id_marketplace: Number(data.id_marketplace),
      id_keywords: Number(data.id_keyword),
      id_commodity: Number(data.id_commodity),
      created_by: Number(data.created_by),
      data,
    });

    return Promise.resolve();
  } catch (error) {
    throw new Error(error.message);
  } finally {
    await page.close();
    await puppeteerPool.release(browser);
  }
};

export default getFromPage;
