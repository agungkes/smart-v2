import {Browser, Page} from 'puppeteer';
import getProduct from './getProduct';
import {performance, PerformanceObserver} from 'perf_hooks';
import dayjs from 'dayjs';
import pMap from 'p-map';
import captchaSolver from './helpers/captchaSolver';
import autoScroll from '../../lib/autoScroll';
import database from '../../config/database';
import puppeteerPool from '../../lib/puppeteerPool';
import screenshot from '../../helpers/screenshot';
import uploadImage from '../../helpers/uploadImage';
import queue from '../../lib/queue';

type GetProductFromPage = {
  browser: Browser;
  page: Page;
  id_marketplace: string | number;
  id_keywords: string | number;
  id_commodity: string | number;
  created_by: number;
  data: JobData;
};

let processing_time = 0;
const observer = new PerformanceObserver(list =>
  list.getEntries().forEach(entry => {
    if (entry.name === 'getProductTimerify') {
      processing_time = entry.duration;
    }
  })
);
observer.observe({buffered: true, entryTypes: ['function']});

const getProductFromPage = async (params: GetProductFromPage) => {
  try {
    // Cek setiap berpindah halaman apakah butuh verifikasi
    const {page, id_marketplace, id_keywords, id_commodity, created_by, data} =
      params;
    await captchaSolver(page);
    await page.waitForTimeout(1500);
    await autoScroll(page);

    const products = await page.evaluate(() => {
      const items = document.querySelectorAll('[data-spm="list"] a');

      return Array.from(items)
        .map(item => {
          const href = item.getAttribute('href');
          const location = document.querySelector('._1KUwM')?.textContent;

          if (href) {
            if (!href.includes('www.lazada.co.id')) {
              return {
                url: `https://www.lazada.co.id${item.getAttribute('href')}`,
                location,
              };
            }

            return {
              url: `https:${href}`,
              location,
            };
          }

          return {
            url: '',
            location: '',
          };
        })
        .map(p => {
          const url = new URL(p.url);

          return {
            ...p,
            url: url.origin + url.pathname,
          };
        })
        .filter(
          (value, index, self) =>
            self.map(s => s.url).indexOf(value.url) === index
        );
    });

    const totalProducts =
      (await page.$eval('._1OXnm', e => e.textContent?.replace(/\D/g, ''))) ||
      0;
    await database.updateProductTotal(
      Number(id_keywords),
      Number(totalProducts)
    );

    await pMap(
      products,
      async ({url, location}) => {
        const browser = await puppeteerPool.acquire();
        const page = await browser.newPage();
        try {
          await page.goto(url, {
            waitUntil: 'networkidle2',
            timeout: 60000,
          });
          const getProductTimerify = performance.timerify(getProduct);
          const product = await getProductTimerify(page);
          if (product) {
            const productId = await database.save({
              store: {
                id_marketplace: Number(id_marketplace),
                url: product.shop.url,
                name: product.shop.name,
                location,
              },
              product: {
                id_keywords: Number(id_keywords),
                id_commodity: Number(id_commodity),
                name: product.name,
                description: product.description,
                category: product.category,
                category_url: product.category_url,
                picture: product.image,
                url: url,
              },
              data_product: {
                price: product.price.toString(),
                crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                rating: Number(product.rating),
                discount: Number(product.discount),
                item_count: product.item_count,
                sold_count: product.sold_count,
                view_count: product.view_count,
                favorited_count: 0,
                processing_time: String(processing_time),
                item_condition: product.condition,
              },
              url,
              created_by,
            });

            const image = await screenshot(page, url);
            await uploadImage(image, productId);
          }
        } catch (error) {
          console.log(error.message);
        } finally {
          await page.close();
          await puppeteerPool.release(browser);
        }
      },
      {
        concurrency: 2,
      }
    );

    const maxPage = Math.ceil(Number(totalProducts) / 40);
    if (
      Number(data.page) + 1 <= maxPage &&
      Number(data.page) <= 100 &&
      data.firstPage
    ) {
      console.log(
        'MENUJU HALAMAN SELANJUTNYA -> ',
        Number(data.page) + 1,
        ' dari total halaman ',
        maxPage
      );

      for (let index = 0; index < (maxPage > 100 ? 99 : maxPage); index++) {
        await queue
          .getQueue(data.query, 'lazada')
          .add(`lazada:${data.location.id}${data.query}:page-${index}`, {
            ...data,
            page: index,
            firstPage: false,
          });
      }
    }
    return Promise.resolve();
  } catch (error) {
    throw new Error(error.message);
  }
};

export default getProductFromPage;
