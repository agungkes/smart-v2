import type {BoundingBox, Page} from 'puppeteer';

type SliderVerification = {
  page: Page;
  handle: BoundingBox;
  slider: BoundingBox;
};
const slideVerification = async (params: SliderVerification) => {
  try {
    const {page, handle, slider} = params;
    await page.mouse.move(
      handle.x + handle.width / 2,
      handle.y + handle.height / 2
    );
    await page.mouse.down();
    await page.mouse.move(
      handle.x + slider.width,
      handle.y + handle.height / 2,
      {
        steps: 10,
      }
    );
    await page.mouse.up();
    await page.waitForTimeout(4000);

    const errorLoading = await page.evaluate(() => {
      if (document.querySelector('.errloading')) {
        return true;
      }

      const isErrorOnIframe = document.querySelector('#sufei-dialog-content');
      if (isErrorOnIframe) {
        const frame =
          // eslint-disable-next-line
          // @ts-ignore
          isErrorOnIframe.contentWindow.document.querySelector('.errloading');
        if (frame) {
          return true;
        }
      }

      return false;
    });

    if (errorLoading) {
      await page.evaluate(() => {
        // eslint-disable-next-line
        // @ts-ignore
        window.__nc.reset();
      });

      await slideVerification({
        page,
        handle,
        slider,
      });
    }

    return true;
  } catch (error) {
    throw new Error(error.message);
  }
};

export default slideVerification;
