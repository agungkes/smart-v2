import {Page} from 'puppeteer';
import slideVerification from '../helpers/slideVerification';

const captchaSolver = async (page: Page) => {
  try {
    const isPunishByBaxia = await page.$('#baxia-punish');
    const isPunishByBaxiaOnIframe = await page.$('#sufei-dialog-content');

    if (!isPunishByBaxia && !isPunishByBaxiaOnIframe) {
      // Tidak butuh verifikasi
      return;
    }

    let slider = null;
    let handle = null;
    if (isPunishByBaxia) {
      const sliderElement = await page.$('#nc_2_n1t');
      if (sliderElement) {
        slider = await sliderElement!.boundingBox();
      }

      const sliderHandleElement = await page.$('#nc_2_n1z');
      if (sliderHandleElement) {
        handle = await sliderHandleElement!.boundingBox();
      }
    }

    if (isPunishByBaxiaOnIframe) {
      const elementHandle = await page.$('#sufei-dialog-content');
      console.log(isPunishByBaxiaOnIframe);

      if (elementHandle) {
        const frame = await elementHandle.contentFrame();
        if (frame) {
          const sliderElement = await frame.$('#nc_2_n1t');
          if (sliderElement) {
            slider = await sliderElement!.boundingBox();
          }

          const sliderHandleElement = await frame.$('#nc_2_n1z');
          if (sliderHandleElement) {
            handle = await sliderHandleElement!.boundingBox();
          }
        }
      }
    }

    if (isPunishByBaxia) {
      if (!slider || !handle) {
        throw new Error('Gagal mendapatkan elemen slider atau handle');
      }
      // // Slide slider
      await slideVerification({
        page,
        slider,
        handle,
      });
    }

    // Berhasil verifikasi
    return;
  } catch (error) {
    throw new Error(error.message);
  }
};

export default captchaSolver;
