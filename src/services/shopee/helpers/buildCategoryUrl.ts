import {ProductApiResponse} from '../types/GetItemApiResponse';

const buildCategoryUrl = (
  name: string,
  index: number,
  catId: number,
  curr: ProductApiResponse['data']['categories']
) => {
  if (name) {
    return `${name.replace(/\s/g, '-')}-cat${index === 0 ? '' : '.'}${curr
      .slice(0, index)
      .map((c: any) => c.catid)
      .join('.')}.${catId}}`;
  }

  return '';
};

export default buildCategoryUrl;
