const normalizePrice = (price: number) => {
  return Number(price.toString().slice(0, -5));
};

export default normalizePrice;
