type GenerateUrl = {
  name: string;
  itemId: string | number;
  shopId: string | number;
};

const buildProductUrl = ({name, itemId, shopId}: GenerateUrl): string => {
  return `https://shopee.co.id/${name.replace(
    /\s/g,
    '-'
  )}-i.${shopId}.${itemId}`;
};

export default buildProductUrl;
