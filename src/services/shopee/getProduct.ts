import dayjs from 'dayjs';
import {Browser} from 'puppeteer';

import {ProductApiResponse} from './types/GetItemApiResponse';
import {ShopeeShop} from '../../types/Shopee';
import {DATE_FORMAT} from '../../helpers/constants';
import autoScroll from '../../lib/autoScroll';
import screenshot from '../../helpers/screenshot';

import buildCategoryUrl from './helpers/buildCategoryUrl';
import buildProductUrl from './helpers/buildProductUrl';
import normalizePrice from './helpers/normalizePrice';
interface Product {
  product: {
    condition: string;
    categories: {
      id: number;
      name: string;
      url: string;
    }[];
    category: string;
    created_at: number;
    description: string;
    name: string;
    historical_sold: number;
    images: string[];
    image: string;
    sold: number;
    rating: number;
    discount: number;
    normal_stock: number;
    discount_stock: number;
    stock: number;
    view_count: number;
    liked_count: number;
    price_before_discount: number;
    price_min: number;
    price_max: number;
    price: number;
    status: string;
    url: string;
  } | null;
  shop: {
    name: string;
    username: string;
    location: string;
    url: string;
    description: string;
    joined_since: string;
    followers: number;
    last_update: string;
    seller_rate: string;
    total_product: number;
  } | null;
  screenshot: string;
}

const getProduct = async (
  browser: Browser,
  productUrl: string
): Promise<Product> => {
  const page = await browser.newPage();
  try {
    let productDetail = null;
    let shop = null;

    await page.setRequestInterception(true);

    page.on('error', err => {
      throw new Error('[GET PRODUCT PAGE]: ' + err.message);
    });

    page.on('request', request => {
      request.continue();
    });

    page.on('response', async response => {
      // Get from product api
      if (response.url().includes('get?itemid')) {
        const res = response.request().response();
        if (res) {
          const {data}: {data: ProductApiResponse['data']} = await res.json();
          productDetail = {
            condition: data.condition === 1 ? 'baru' : 'bekas',
            categories: data.categories.map((category, idx, curr) => ({
              id: category.catid,
              name: category.display_name,
              url: `https://shopee.co.id/${buildCategoryUrl(
                category.display_name,
                idx,
                category.catid,
                curr
              )}`,
            })),
            category: data.categories
              .map(category => category.display_name)
              .join('|'),
            created_at: data.ctime,
            description: data.description,
            name: data.name ?? '',
            historical_sold: data.historical_sold,
            images: data.images.map(
              image => `https://cf.shopee.co.id/file/${image}`
            ),
            image: `https://cf.shopee.co.id/file/${data.image}`,
            sold: data.sold,
            rating: data.item_rating.rating_star,
            discount: data.show_discount,
            normal_stock: data.normal_stock,
            discount_stock: data.discount_stock,
            stock: data.stock,
            view_count: 0,
            liked_count: data.liked_count,
            price_before_discount: normalizePrice(data.price_before_discount),
            price_min: normalizePrice(data.price_min),
            price_max: normalizePrice(data.price_max),
            price: normalizePrice(data.price),
            status: data.item_status,
            url: buildProductUrl({
              name: data.name ?? '',
              itemId: data.itemid,
              shopId: data.shopid,
            }),
          };
        }
      }

      if (response.url().includes('get_shop_info')) {
        const res = response.request().response();
        if (res) {
          const {data}: {data: ShopeeShop['data']} = await res.json();
          shop = {
            name: data.name ?? '',
            username: data.account.username,
            location: data.place ?? data.shop_location,
            url: `https://shopee.co.id/${data.account.username}`,
            description: data.description,
            joined_since: dayjs(data.ctime).format(DATE_FORMAT),
            followers: data.follower_count,
            last_update: dayjs(data.last_active_time).format(DATE_FORMAT),
            seller_rate:
              (data.rating_star && data.rating_star.toString()) || '0',
            total_product: data.item_count,
          };
        }
      }
    });

    await page.goto(productUrl, {
      waitUntil: 'networkidle2',
    });
    await autoScroll(page);

    const screenshotPath = await screenshot(page, productUrl);

    await page.close();
    return {
      product: productDetail,
      shop,
      screenshot: screenshotPath,
    };
  } catch (error) {
    await page.close();
    throw new Error('[GET PRODUCT]: ' + error.message);
  }
};

export default getProduct;
