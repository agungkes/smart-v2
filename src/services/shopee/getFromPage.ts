import getProduct from './getProduct';
import pMap from 'p-map';
import autoScroll from '../../lib/autoScroll';
import puppeteerPool from '../../lib/puppeteerPool';
import dayjs from 'dayjs';
import {Job} from 'bullmq';
import {URLSearchParams} from 'url';
import database from '../../config/database';
import uploadImage from '../../helpers/uploadImage';
import queue from '../../lib/queue';

const BLOCKED_RESOURSE = ['image', 'fonts'];

const getFromPage = async ({data}: Job<JobData>) => {
  console.log('[shopee] Sedang mengambil pada halaman: ', data.page);
  const puppeteer = await puppeteerPool.acquire();

  const page = await puppeteer.newPage();

  try {
    let target = 0;
    await page.setRequestInterception(true);
    // Handling error
    page.on('error', err => {
      throw new Error('[GET PRODUCT FROM PAGE2]: ' + err.message);
    });

    page.on('request', request => {
      if (BLOCKED_RESOURSE.some(b => request.resourceType().includes(b)))
        request.abort();
      else request.continue();
    });

    page.on('response', async response => {
      // Get from search item api
      if (response.url().includes('search_items')) {
        const data = response.request().response();
        if (data) {
          const json = await data.json();
          target = json.total_count;
        }
      }
    });

    const query = new URLSearchParams();
    query.set('complementKeyword', String(data.query).replace(/\+/g, '%20'));
    query.set('locations', encodeURI(data.location.name));
    query.set('keyword', String(data.query).replace(/\s/g, ''));
    query.set('page', String(data.page));

    await page.goto(`https://shopee.co.id/search?${query.toString()}`, {
      waitUntil: 'networkidle2',
    });

    if (!data.firstPage) {
      await autoScroll(page);
      const productUrls = await page.evaluate(() => {
        const productElement = document.querySelectorAll('[data-sqe="link"]');
        return Array.from(productElement).map(elem => {
          const url = new URL(
            `https://shopee.co.id${elem.getAttribute('href')}`
          );

          return url.origin + url.pathname;
        });
      });

      await pMap(
        productUrls,
        async productUrl => {
          try {
            const detail = await getProduct(puppeteer as any, productUrl);
            const {product, shop, screenshot} = detail;
            if (shop && product) {
              const url = new URL(productUrl);
              const productId = await database.save({
                created_by: Number(data.created_by),
                url: `${url.origin}${url.pathname}`,
                store: {
                  seller_rate: shop.seller_rate,
                  id_marketplace: 6,
                  url: shop.url,
                  name: shop.name,
                  location: shop.location,
                  description: shop.description,
                  joined_since: shop.joined_since,
                  followers: shop.followers,
                  last_update: shop.last_update,
                  total_product: shop.total_product,
                  target,
                },
                product: {
                  id_keywords: Number(data.id_keyword),
                  id_commodity: Number(data.id_commodity),
                  name: product.name,
                  description: product.description,
                  category: product.categories.map(c => c.name).join('|'),
                  category_url: product.categories.map(c => c.url).join('|'),
                  picture: product.image,
                  url: product.url,
                },
                data_product: {
                  price: product.price.toString(),
                  crawled: dayjs().format('YYYY-MM-DD HH:mm:ss'),
                  rating: product.rating,
                  discount: product.discount,
                  item_count: product.stock,
                  sold_count: product.sold,
                  view_count: product.view_count,
                  favorited_count: 50,
                  item_condition: product.condition,
                  // processing_time: String(processing_time),
                },
              });

              await uploadImage(screenshot, productId, true);
            }
          } catch (error) {
            throw new Error('[' + productUrl + ']: ' + error.message);
          }
        },
        {
          concurrency: 4,
        }
      );
    }

    const maxPage = Math.ceil(Number(target) / 60);

    if (
      Number(data.page) <= maxPage &&
      Number(data.page) <= 100 &&
      data.firstPage
    ) {
      console.log('MASIH ADA HALAMAN SELANJUTNYA');

      for (let index = 0; index < (maxPage > 100 ? 100 : maxPage); index++) {
        await queue
          .getQueue(data.query, 'shopee')
          .add(`shopee:${data.location.id}${data.query}:page-${index}`, {
            ...data,
            page: index,
            firstPage: false,
          });
      }

      return Promise.resolve();
    }

    return Promise.resolve(`Selesai melakukan crawling pada: ${data.page}`);
  } catch (error) {
    throw new Error('[GET PRODUCT FROM PAGE]: ' + error.message);
  } finally {
    await page.close();
    await puppeteerPool.release(puppeteer);
  }
};

export default getFromPage;
