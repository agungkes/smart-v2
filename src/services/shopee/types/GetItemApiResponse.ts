export interface ProductApiResponse {
  data: Data;
  error: null;
  error_msg: null;
}

interface Data {
  itemid: number;
  shopid: number;
  userid: number;
  price_max_before_discount: number;
  has_lowest_price_guarantee: boolean;
  price_before_discount: number;
  price_min_before_discount: number;
  exclusive_price_info: null;
  hidden_price_display: null;
  price_min: number;
  price_max: number;
  price: number;
  stock: number;
  discount: string;
  historical_sold: number;
  sold: number;
  show_discount: number;
  raw_discount: number;
  min_purchase_limit: number;
  overall_purchase_limit: OverallPurchaseLimit;
  pack_size: null;
  is_live_streaming_price: null;
  name: string;
  ctime: number;
  item_status: string;
  status: number;
  condition: number;
  catid: number;
  description: string;
  is_mart: boolean;
  show_shopee_verified_label: boolean;
  size_chart: string;
  reference_item_id: string;
  brand: null;
  item_rating: ItemRating;
  label_ids: number[];
  attributes: Attribute[];
  liked: boolean;
  liked_count: number;
  cmt_count: number;
  flag: number;
  shopee_verified: boolean;
  is_adult: boolean;
  is_preferred_plus_seller: boolean;
  tier_variations: TierVariation[];
  bundle_deal_id: number;
  can_use_bundle_deal: boolean;
  add_on_deal_info: null;
  bundle_deal_info: BundleDealInfo;
  can_use_wholesale: boolean;
  wholesale_tier_list: any[];
  is_group_buy_item: null;
  group_buy_info: null;
  welcome_package_type: number;
  welcome_package_info: null;
  images: string[];
  image: string;
  video_info_list: VideoInfoList[];
  item_type: number;
  is_official_shop: boolean;
  show_official_shop_label_in_title: boolean;
  shop_location: string;
  coin_earn_label: null;
  cb_option: number;
  is_pre_order: boolean;
  estimated_days: number;
  badge_icon_type: number;
  show_free_shipping: boolean;
  shipping_icon_type: number;
  cod_flag: number;
  show_original_guarantee: boolean;
  categories: Category[];
  other_stock: number;
  item_has_post: boolean;
  discount_stock: number;
  current_promotion_has_reserve_stock: boolean;
  current_promotion_reserved_stock: number;
  normal_stock: number;
  brand_id: number;
  is_alcohol_product: boolean;
  show_recycling_info: boolean;
  coin_info: CoinInfo;
  models: Model[];
  spl_info: null;
  preview_info: null;
  is_cc_installment_payment_eligible: boolean;
  is_non_cc_installment_payment_eligible: boolean;
  flash_sale: null;
  upcoming_flash_sale: null;
  deep_discount: null;
  has_low_fulfillment_rate: boolean;
  is_partial_fulfilled: boolean;
  makeups: null;
}

interface Attribute {
  name: string;
  value: string;
  id: number;
  is_timestamp: boolean;
  brand_option: null;
  val_id: null;
}

interface BundleDealInfo {
  bundle_deal_id: number;
  bundle_deal_label: string;
}

interface Category {
  catid: number;
  display_name: string;
  no_sub: boolean;
  is_default_subcat: boolean;
}

interface CoinInfo {
  spend_cash_unit: number;
  coin_earn_items: any[];
}

interface ItemRating {
  rating_star: number;
  rating_count: number[];
}

interface Model {
  itemid: number;
  status: number;
  current_promotion_reserved_stock: number;
  name: string;
  promotionid: number;
  price: number;
  price_stocks: PriceStock[];
  current_promotion_has_reserve_stock: boolean;
  normal_stock: number;
  extinfo: Extinfo;
  price_before_discount: number;
  modelid: number;
  stock: number;
  has_gimmick_tag: boolean;
}

interface Extinfo {
  tier_index: number[];
  group_buy_info: null;
}

interface PriceStock {
  allocated_stock: number | null;
  stock_breakdown_by_location: null;
}

interface OverallPurchaseLimit {
  order_max_purchase_limit: number;
  overall_purchase_limit: null;
  item_overall_quota: null;
  start_date: null;
  end_date: null;
}

interface TierVariation {
  name: string;
  options: string[];
  images: string[] | null;
  properties: null;
  type: number;
}

interface VideoInfoList {
  video_id: string;
  thumb_url: string;
  duration: number;
  version: number;
  formats: any[];
  default_format: null;
}
