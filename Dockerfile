FROM node:14 as base

WORKDIR /src
COPY package*.json /
COPY yarn.lock ./

EXPOSE 3000

FROM base as production
ENV NODE_ENV=production
RUN yarn set version berry && yarn set version latest

COPY . /
CMD [ "node", "build/index.js" ]

FROM base as dev
ENV NODE_ENV=development
RUN yarn set version berry
RUN yarn set version latest

RUN npm install -g nodemon && yarn install
COPY . /
CMD ["nodemon", "src/index.ts"]
