#!/bin/bash

yarn unplug gts
eslintrc=$(find .yarn -name .eslintrc.json | sed -n 2p)
prettierrc=$(find .yarn -name .prettierrc.json)
tsconfig=$(find .yarn -name tsconfig-google.json)

echo "{
  \"extends\": \"./$eslintrc\"
}" > .eslintrc.json

echo "module.exports = {
  ...require('./$prettierrc')
}
" > .prettierrc.js

echo "{
  \"extends\": \"./$tsconfig\",
  \"compilerOptions\": {
    \"rootDir\": \".\",
    \"outDir\": \"build\"
  },
  \"include\": [
    \"src/**/*.ts\",
    \"test/**/*.ts\"
  ]
}" > tsconfig.json